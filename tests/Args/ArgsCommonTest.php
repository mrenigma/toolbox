<?php
namespace Soapbox\Toolbox\Tests\Args;

require_once __DIR__ . '/../../vendor/autoload.php';

use Soapbox\Toolbox\Args\ArgsCommon;

class ArgsCommonTest extends \PHPUnit_Framework_TestCase
{

    public function testBuildPassedVars()
    {

        $ArgsCommon = new ArgsCommon();

        $passed_vars = $ArgsCommon->buildPassedVars([
            'select-post_type'    => [
                0 => 'post',
            ],
            'select-year'         => [
                0 => '2015',
            ],
            'hidden-s'            => 'Hello',
            'hidden-current-page' => '1'
        ]);

        $this->assertNotEmpty($passed_vars);
        $this->isType('array');
    }

    public function testGetPassedVar()
    {

        // Nothing to test, it just returns if found!
    }

    public function addPassedVar()
    {

        // Nothing to test, it just returns if added!
    }
}
