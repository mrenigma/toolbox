<?php
namespace Soapbox\Toolbox\Tests\Args;

require_once __DIR__ . '/../../vendor/autoload.php';

use Soapbox\Toolbox\Args\ArgsBuilder;
use Soapbox\Toolbox\Models\WordPress;
use Soapbox\Toolbox\Views\Path;

class ArgsBuilderTest extends \PHPUnit_Framework_TestCase
{

    public function providerIncludes()
    {

        $includes = [
            'pagination_id'                        => 'current-page',
            'posts_per_page'                       => '10',
            'buttons'                              => [
                0 => 'reset',
            ],
            'types_include_or_exclude'             => 'include',
            'types'                                => [
                0 => 'country',
                1 => 'event',
                2 => 'multimedia',
                3 => 'news-item',
                4 => 'person',
                5 => 'post',
                6 => 'project',
                7 => 'publication',
            ],
            'types_label'                          => 'Content Type',
            'post_option_label'                    => 'Blog Posts',
            'publication_children_filter_slug'     => 'publication-types',
            'publication_add_children_slugs'       => [
                0 => 'issue-briefing',
                1 => 'working-paper',
            ],
            'publication_add_children_names'       => [
                0 => 'Issue Briefings',
                1 => 'Working Papers',
            ],
            'multimedia_children_filter_slug'      => 'media-types',
            'multimedia_add_children_slugs'        => [
                0 => 'audio',
                1 => 'video',
            ],
            'multimedia_add_children_names'        => [
                0 => 'Audio',
                1 => 'Video',
            ],
            'years_label'                          => 'Date',
            'years_include_or_exclude'             => 'exclude',
            'years'                                => [
                0 => '',
            ],
            'country-cat_include_or_exclude'       => 'exclude',
            'country-cat_terms'                    => [
                0 => '',
            ],
            'research-themes_include_or_exclude'   => 'exclude',
            'research-themes_terms'                => [
                0 => '',
            ],
            'publication-types_include_or_exclude' => 'exclude',
            'publication-types_terms'              => [
                0 => '',
            ],
            'media-types_include_or_exclude'       => 'exclude',
            'media-types_terms'                    => [
                0 => '',
            ]
        ];

        return [
            [$includes]
        ];
    }

    /**
     * @param array|null $includes Arguments to include in generation
     *
     * @dataProvider providerIncludes
     */
    public function testBuildArgs($includes)
    {

        $this->assertNotEmpty($includes);
        $ArgsBuilder = new ArgsBuilder(new WordPress(), new Path());
        $args        = $ArgsBuilder->buildArgs($includes);

        $this->assertNotEmpty($args);
        $this->assertArrayHasKey('post_type', $args);
    }
}
