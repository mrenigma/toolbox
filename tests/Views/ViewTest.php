<?php

namespace Soapbox\Toolbox\Tests\Views;

require_once __DIR__ . '/../../vendor/autoload.php';

use Soapbox\Toolbox\Interfaces\ViewInterface;
use Soapbox\Toolbox\Views\Path;
use Soapbox\Toolbox\Views\View;

/**
 * Class ViewTest
 *
 * @package Soapbox\Toolbox\Tests\Views
 */
class ViewTest extends \PHPUnit_Framework_TestCase
{

    public function viewProvider()
    {

        $View = [
            new View(new Path(), 'buttons', [__DIR__ . '/../../src/Filters/FilterTemplates/'], '.php')
        ];

        return [
            $View,
            $View,
            $View,
            $View
        ];
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface $View
     *
     * @dataProvider viewProvider
     */
    public function testSetPath(ViewInterface $View)
    {

        $test = $View->setPath('filter-element-button', [__DIR__ . '/../../src/Filters/FilterTemplates/FormElements/'], '.php');

        $this->assertEquals(realpath(__DIR__ . '/../../src/Filters/FilterTemplates/FormElements/filter-element-button.php'), $test);
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface $View
     *
     * @dataProvider viewProvider
     */
    public function testSetLayout(ViewInterface $View)
    {

        $test = $View->setLayout('filters', [__DIR__ . '/../../src/Filters/FilterTemplates/'], '/php');

        $this->assertEquals(realpath(__DIR__ . '/../../src/Filters/FilterTemplates/FormElements/filters.php'), $test);
    }

    public function testSetVars()
    {
        // Nothing to test, it just sets the private property!
    }

    public function testSetLayoutVars()
    {

        // Nothing to test, it just sets the private property!
    }

    public function testSetVar()
    {

        // Nothing to test, it just sets the private property!
    }

    public function testSetLayoutVar()
    {

        // Nothing to test, it just sets the private property!
    }

    public function hasVars()
    {

        // Nothing to test, it just returns the private property!
    }

    public function hasLayoutVars()
    {

        // Nothing to test, it just returns the private property!
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface $View
     *
     * @dataProvider viewProvider
     */
    public function output(ViewInterface $View)
    {

        $html = $View->output();

        $this->stringContains('<input', false);

    }

    public function testHasOutput()
    {

        // Nothing to test, it just returns the private property!
    }

    public function testHasLayoutOutput()
    {

        // Nothing to test, it just returns the private property!
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface $View
     *
     * @depends      testSetPath
     * @dataProvider viewProvider
     */
    public function reset(ViewInterface $View)
    {

        $ViewReset  = clone $View;
        $ViewReset  = $View->reset();
        $ViewReset2 = clone $View;
        $ViewReset2 = $View->reset([]);

        $this->assertAttributeEquals(realpath(__DIR__ . '/../../src/Filters/FilterTemplates/FormElements/filter-element-button.php'), 'path', $ViewReset);
        $this->assertAttributeEmpty('path', $ViewReset2);
    }
}
