<?php

namespace Soapbox\Toolbox\Tests\Views;

use Soapbox\Toolbox\Interfaces\PathInterface;
use Soapbox\Toolbox\Views\Path;

require_once __DIR__ . '/../../vendor/autoload.php';

class PathTest extends \PHPUnit_Framework_TestCase
{

    public function pathProvider()
    {

        $Path = [
            new Path('test', __DIR__ . '/../Configs/', '.php', __DIR__ . '/../Configs/')
        ];

        return [
            $Path,
            $Path,
            $Path,
            $Path,
            $Path,
            $Path,
            $Path
        ];
    }

    public function testGetExt()
    {

        // Nothing to test, it just sets the private property!
    }

    public function testSetExt()
    {

        // Nothing to test, it just sets the private property!
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path
     *
     * @dataProvider pathProvider
     */
    public function testGetPath(PathInterface $Path)
    {

        $this->assertNotEmpty($Path->getPath());
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path
     *
     * @dataProvider pathProvider
     */
    public function testGetPaths(PathInterface $Path)
    {

        $test = $Path->getPaths();
        $this->assertNotEmpty($test);
        $this->isType('array');
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path
     *
     * @depends      testCheckPath
     * @dataProvider pathProvider
     */
    public function testGetRealPath(PathInterface $Path)
    {

        $test = $Path->getRealPath();

        $this->assertNotEmpty($test);
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path
     *
     * @dataProvider pathProvider
     */
    public function testCheckPath(PathInterface $Path)
    {

        $test = $Path->checkPath();

        $this->isTrue();
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path
     *
     * @dataProvider pathProvider
     */
    public function testCheckPaths(PathInterface $Path)
    {

        $test = $Path->checkPaths();

        $this->isTrue();
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path
     *
     * @dataProvider pathProvider
     */
    public function testGetThemePath(PathInterface $Path)
    {

        $test  = $Path->getThemePath();
        $test2 = $Path->getThemePath(__DIR__);

        $this->assertNotEmpty($test);
        $this->assertEquals(realpath(__DIR__), $test2);
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path
     *
     * @dataProvider pathProvider
     */
    public function testWithinThemes(PathInterface $Path)
    {

        $test = $Path->withinThemes();

        $this->isTrue();
    }
}
