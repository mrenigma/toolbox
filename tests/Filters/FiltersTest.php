<?php

namespace Soapbox\Toolbox\Tests\Filters;

require_once __DIR__ . '/../../vendor/autoload.php';

use Soapbox\Toolbox\Filters\Filters;
use Soapbox\Toolbox\Interfaces\FiltersInterface;
use Soapbox\Toolbox\Models\WordPress;
use Soapbox\Toolbox\Views\Path;
use Soapbox\Toolbox\Views\View;

class FiltersTest extends \PHPUnit_Framework_TestCase
{

    public static function filterProvider()
    {

        $Filter = new Filters(new WordPress(), new Path(), new View(new Path()), [], [], [], __DIR__ . '/../../src/Filters/FilterTemplates/');
        $Filter = $Filter->addPath(new Path('FilterTemplates/', [], '', __DIR__ . '/../../src/Filters/'));
        $Filter = [$Filter];

        return [
            $Filter,
            $Filter
        ];
    }

    public function testAddPath()
    {

        $path_1 = 'assets/includes/vendor/sb-dev-team/toolbox/src/Filters/FilterTemplates';
        $path_2 = 'assets/includes/Filters/FilterTemplates';

        $template_paths = [
            'assets/includes/vendor/sb-dev-team/toolbox/src/Filters/FilterTemplates',
            'assets/includes/Toolbox/src/Filters/FilterTemplates/'
        ];

        if (!in_array($path_1, $template_paths)) {
            $template_paths[] = $path_1;
        }

        $this->assertArrayHasKey($path_1, array_combine(array_values($template_paths), array_values($template_paths)));

        if (!in_array($path_2, $template_paths)) {
            $template_paths[] = $path_2;
        }

        $this->assertArrayHasKey($path_2, array_combine(array_values($template_paths), array_values($template_paths)));
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\FiltersInterface $Filter
     *
     * @dataProvider filterProvider
     */
    public function testBuildFilters(FiltersInterface $Filter)
    {

        $includes = [
            'pagination_id'                        => 'current-page',
            'posts_per_page'                       => '10',
            'buttons'                              => [
                0 => 'reset',
            ],
            'types_include_or_exclude'             => 'include',
            'types'                                => [
                0 => 'country',
                1 => 'event',
                2 => 'multimedia',
                3 => 'news-item',
                4 => 'person',
                5 => 'post',
                6 => 'project',
                7 => 'publication',
            ],
            'types_label'                          => 'Content Type',
            'post_option_label'                    => 'Blog Posts',
            'publication_children_filter_slug'     => 'publication-types',
            'publication_add_children_slugs'       => [
                0 => 'issue-briefing',
                1 => 'working-paper',
            ],
            'publication_add_children_names'       => [
                0 => 'Issue Briefings',
                1 => 'Working Papers',
            ],
            'multimedia_children_filter_slug'      => 'media-types',
            'multimedia_add_children_slugs'        => [
                0 => 'audio',
                1 => 'video',
            ],
            'multimedia_add_children_names'        => [
                0 => 'Audio',
                1 => 'Video',
            ],
            'years_label'                          => 'Date',
            'years_include_or_exclude'             => 'exclude',
            'years'                                => [
                0 => '',
            ],
            'country-cat_include_or_exclude'       => 'exclude',
            'country-cat_terms'                    => [
                0 => '',
            ],
            'research-themes_include_or_exclude'   => 'exclude',
            'research-themes_terms'                => [
                0 => '',
            ],
            'publication-types_include_or_exclude' => 'exclude',
            'publication-types_terms'              => [
                0 => '',
            ],
            'media-types_include_or_exclude'       => 'exclude',
            'media-types_terms'                    => [
                0 => '',
            ]
        ];

        $filters = $Filter->buildFilters(['years'], $includes);

        $this->assertNotEmpty($filters);
        $this->stringContains('<label', false);
    }

    /**
     * @param \Soapbox\Toolbox\Interfaces\FiltersInterface $Filter
     *
     * @dataProvider filterProvider
     */
    public function testBuildSorters(FiltersInterface $Filter)
    {

        $sorters = $Filter->buildSorters();

        $this->assertNotEmpty($sorters);
        $this->stringContains('<label', false);
    }

    public function testClearHtml()
    {
        // Nothing to test, it just empties class variables!
    }
}
