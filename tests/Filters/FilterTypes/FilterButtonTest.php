<?php

namespace Soapbox\Toolbox\Tests\Filters\FilterTypes;

require_once __DIR__ . '/../../../vendor/autoload.php';

use Soapbox\Toolbox\Filters\FilterTypes\FilterButton;
use Soapbox\Toolbox\Models\WordPress;
use Soapbox\Toolbox\Views\Path;
use Soapbox\Toolbox\Views\View;

class FilterButtonTest extends \PHPUnit_Framework_TestCase
{

    public function testBuild()
    {

        $Model    = new WordPress();
        $View     = new View(new Path(), __DIR__ . '/../../../src/Filters/FilterTemplates/buttons.php', [__DIR__ . '/../../../src/Filters/FilterTemplates/']);
        $includes = [
            'pagination_id'                        => 'current-page',
            'posts_per_page'                       => '10',
            'buttons'                              => [
                0 => 'reset',
            ],
            'types_include_or_exclude'             => 'include',
            'types'                                => [
                0 => 'country',
                1 => 'event',
                2 => 'multimedia',
                3 => 'news-item',
                4 => 'person',
                5 => 'post',
                6 => 'project',
                7 => 'publication',
            ],
            'types_label'                          => 'Content Type',
            'post_option_label'                    => 'Blog Posts',
            'publication_children_filter_slug'     => 'publication-types',
            'publication_add_children_slugs'       => [
                0 => 'issue-briefing',
                1 => 'working-paper',
            ],
            'publication_add_children_names'       => [
                0 => 'Issue Briefings',
                1 => 'Working Papers',
            ],
            'multimedia_children_filter_slug'      => 'media-types',
            'multimedia_add_children_slugs'        => [
                0 => 'audio',
                1 => 'video',
            ],
            'multimedia_add_children_names'        => [
                0 => 'Audio',
                1 => 'Video',
            ],
            'years_label'                          => 'Date',
            'years_include_or_exclude'             => 'exclude',
            'years'                                => [
                0 => '',
            ],
            'country-cat_include_or_exclude'       => 'exclude',
            'country-cat_terms'                    => [
                0 => '',
            ],
            'research-themes_include_or_exclude'   => 'exclude',
            'research-themes_terms'                => [
                0 => '',
            ],
            'publication-types_include_or_exclude' => 'exclude',
            'publication-types_terms'              => [
                0 => '',
            ],
            'media-types_include_or_exclude'       => 'exclude',
            'media-types_terms'                    => [
                0 => '',
            ]
        ];

        $return = FilterButton::build($Model, $View, $includes, [], [], '', '');

        $this->assertNotEmpty($return->html);
        $this->assertThat($return->html, $this->stringContains('<input', false));
        $this->assertNotEmpty($return->rendered_vars);
        $this->assertThat($return->rendered_vars, $this->contains('buttons'));
    }
}
