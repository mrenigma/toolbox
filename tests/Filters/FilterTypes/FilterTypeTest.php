<?php

namespace Soapbox\Toolbox\Tests\Filters\FilterTypes;

require_once __DIR__ . '/../../../vendor/autoload.php';

use Soapbox\Toolbox\Filters\FilterTypes\FilterType;
use Soapbox\Toolbox\Models\WordPress;

class FilterTypeTest extends \PHPUnit_Framework_TestCase
{

    public function includesProvider()
    {

        $includes = [
            [
                'pagination_id'                        => 'current-page',
                'posts_per_page'                       => '10',
                'buttons'                              => [
                    0 => 'reset',
                ],
                'types_include_or_exclude'             => 'include',
                'types'                                => [
                    0 => 'country',
                    1 => 'event',
                    2 => 'multimedia',
                    3 => 'news-item',
                    4 => 'person',
                    5 => 'post',
                    6 => 'project',
                    7 => 'publication',
                ],
                'types_label'                          => 'Content Type',
                'post_option_label'                    => 'Blog Posts',
                'publication_children_filter_slug'     => 'publication-types',
                'publication_add_children_slugs'       => [
                    0 => 'issue-briefing',
                    1 => 'working-paper',
                ],
                'publication_add_children_names'       => [
                    0 => 'Issue Briefings',
                    1 => 'Working Papers',
                ],
                'multimedia_children_filter_slug'      => 'media-types',
                'multimedia_add_children_slugs'        => [
                    0 => 'audio',
                    1 => 'video',
                ],
                'multimedia_add_children_names'        => [
                    0 => 'Audio',
                    1 => 'Video',
                ],
                'years_label'                          => 'Date',
                'years_include_or_exclude'             => 'exclude',
                'years'                                => [
                    0 => '',
                ],
                'country-cat_include_or_exclude'       => 'exclude',
                'country-cat_terms'                    => [
                    0 => '',
                ],
                'research-themes_include_or_exclude'   => 'exclude',
                'research-themes_terms'                => [
                    0 => '',
                ],
                'publication-types_include_or_exclude' => 'exclude',
                'publication-types_terms'              => [
                    0 => '',
                ],
                'media-types_include_or_exclude'       => 'exclude',
                'media-types_terms'                    => [
                    0 => '',
                ]
            ]
        ];

        return [
            $includes,
            $includes
        ];
    }

    public function testGetQueryMethodName()
    {

        $test = FilterType::getQueryMethodName('this-is-a-slug');

        $this->assertEquals('getThisIsASlugQuery', $test);
    }

    public function testGetFilterLabel()
    {

        $test  = FilterType::getFilterLabel(['test_label' => 'This is a test'], 'test', 'This is a default');
        $test2 = FilterType::getFilterLabel(['test_label' => 'This is a test'], 'check', 'This is a default');

        $this->assertEquals('This is a test', $test);
        $this->assertEquals('This is a default', $test2);
    }

    public function testGetPostTypes()
    {

        $Model = new WordPress();

        $test = FilterType::getPostTypes($Model);

        $this->isType('array');
    }

    /**
     * @param array $includes
     *
     * @dataProvider includesProvider
     */
    public function testGetTypesExclude(Array $includes)
    {

        $Model = new WordPress();
        $test  = FilterType::getTypesExclude(FilterType::getPostTypes($Model), $includes);

        $this->isType('array');
    }

    public function testGetFilterOptionLabel()
    {

        $test  = FilterType::getFilterOptionLabel(['test_option_label' => 'This is a test'], 'test', 'This is a default');
        $test2 = FilterType::getFilterOptionLabel(['test_option_label' => 'This is a test'], 'check', 'This is a default');

        $this->assertEquals('This is a test', $test);
        $this->assertEquals('This is a default', $test2);
    }

    public function testGetIniString()
    {

        $test  = FilterType::getIniString(['types_label' => 'Content Type'], 'types_label', 'No type label');
        $test2 = FilterType::getIniString(['types_label' => 'Content Type'], 'check_label', 'No type label');

        $this->assertEquals('Content Type', $test);
        $this->assertEquals('No type label', $test2);
    }

    public function testGetChildrenFilterSlug()
    {

        $test  = FilterType::getChildrenFilterSlug(['test_children_filter_slug' => 'test-slug'], 'test', 'no-slug');
        $test2 = FilterType::getChildrenFilterSlug(['test_children_filter_slug' => 'test-slug'], 'check', 'no-slug');

        $this->assertEquals('test-slug', $test);
        $this->assertEquals('no-slug', $test2);
    }

    /**
     * @param array $includes
     *
     * @dataProvider includesProvider
     */
    public function testGetFilterCustomChildren(Array $includes)
    {

        $test    = FilterType::getFilterCustomChildren($includes, 'publication');
        $compare = array_combine($includes['publication_add_children_slugs'], $includes['publication_add_children_names']);

        $this->assertEquals($compare, $test);

    }
}
