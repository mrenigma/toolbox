<?php

namespace Soapbox\Toolbox\Tests\Configs;

require_once __DIR__ . '/../../vendor/autoload.php';

use Soapbox\Toolbox\Configs\Config;
use Soapbox\Toolbox\Views\Path;

class ConfigTest extends \PHPUnit_Framework_TestCase
{

    public function testGetConfig()
    {

        $types = [
            'ini',
            'array',
            'xml',
            'json'
        ];

        foreach ($types as $type) {
            if ($type === 'array') {
                $ext = '.php';
            } else {
                $ext = '.' . $type;
            }

            $Path   = new Path('test', [__DIR__], $ext, __DIR__);
            $Config = new Config($Path, $ext);

            if ($Path->checkPath()) {
                $path = $Path->getRealPath();
            } else {
                $path = '';
            }

            $this->assertFileExists($path);

            $parse = $Config->getConfig($ext);

            $this->isType('array');
            $this->assertArrayHasKey('glossary', $parse);
        }
    }

    public function testGet()
    {

        $Path   = new Path('get-test', __DIR__, '.ini', __DIR__);
        $Config = new Config($Path);

        $get = $Config->get(['blog' => 'pagination_id']);

        $this->assertNotEmpty($get);
        $this->assertEquals('current-page', $get);
    }

    public function testGetAll()
    {

        $Path   = new Path('get-test', __DIR__, '.ini', __DIR__);
        $Config = new Config($Path);

        $get_all = $Config->getAll();

        $this->assertNotEmpty($get_all);
        $this->isType('array');
    }
}
