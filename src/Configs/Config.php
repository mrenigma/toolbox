<?php
namespace Soapbox\Toolbox\Configs;

use Soapbox\Toolbox\Interfaces\ConfigInterface;
use Soapbox\Toolbox\Interfaces\PathInterface;

/**
 * Class Config
 * A class to process different files into a config array.
 * This config array is then able to be read but not added to.
 *
 * @package Soapbox\Toolbox\Configs
 */
class Config implements ConfigInterface
{

    /**
     * @var string $path Location of config file
     */
    protected $path;

    /**
     * @var array $config Store of config data
     */
    protected $config;

    /**
     * Add the passed path to the $path private variable
     * Then get the config file accordingly
     *
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path Location of config file
     * @param string                                    $type Type of config file
     */
    public function __construct(PathInterface $Path, $type = 'ini')
    {

        // Normalise the type
        if ($type === 'array') {
            $type = 'php';
        }

        // Check the Path type
        if ($Path->getExt() !== $type) {
            $Path->setExt($type);
        }

        // Check path exists
        if ($Path->checkPath()) {
            $this->path = $Path->getRealPath();
        }

        $this->config = $this->getConfig($type);
    }

    /**
     * Get the config file contents from the $_path
     * Process according to the type
     *
     * @param string $type Type of config file
     *
     * @return array Array of configuration data
     */
    public function getConfig($type = 'ini')
    {

        if (strpos($type, '.') === 0) {
            $type = substr($type, 1);
        }

        switch ($type) {
            case 'ini':
                return parse_ini_file($this->path, true);

                break;
            case 'array':
            case 'php':
                /** @noinspection PhpIncludeInspection */

                return include($this->path);

                break;
            case 'xml':
                return json_decode(json_encode(simplexml_load_file($this->path), JSON_NUMERIC_CHECK), true);

                break;
            case 'json':
                return json_decode(file_get_contents($this->path), true);
        }

        return [];
    }

    /**
     * Get the value based on the key.
     * You can use an associative array to retrieve deep values as needed
     *
     * @param string|array $key    The key of the value to find
     * @param array        $values The sub values to process if going deep
     *
     * @return string|null Return the value if found
     */
    public function get($key, $values = [])
    {

        $value = null;

        if (is_array($key)) {
            $new_values = $this->get(array_keys($key)[0]);
            $a_key      = array_shift($key);

            $value = $this->get($a_key, $new_values);
        } else if (!empty($values) && isset($values[$key])) {
            $value = $values[$key];
        } else {
            $value = $this->config[$key];
        }

        return $value;
    }

    /**
     * Get all the values found in the config
     *
     * @return array
     */
    public function getAll()
    {

        return $this->config;
    }
}
