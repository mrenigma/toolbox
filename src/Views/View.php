<?php
namespace Soapbox\Toolbox\Views;

use Soapbox\Toolbox\Interfaces\PathInterface;
use Soapbox\Toolbox\Interfaces\ViewInterface;

/**
 * Class View
 * A class to separate HTML from OOP/procedural PHP.
 * Pass this class the template location and the variables for it to then generate the template HTML.
 *
 * @package Soapbox\Toolbox\Views
 * @see     Soapbox\Toolbox\Interfaces\ViewInterface
 * @see     Soapbox\Toolbox\Views\Path
 */
class View implements ViewInterface
{

    /**
     * @var array $passed_vars Array of variables to use within the view
     */
    protected $passed_vars = [];

    /**
     * @var array $layout_vars Array of variables to use within the layout
     */
    protected $layout_vars = [];

    /**
     * @var \Soapbox\Toolbox\Interfaces\PathInterface $Path The class to verify the set paths
     */
    protected $Path;

    /**
     * @var string $path Location of view
     */
    protected $path;

    /**
     * @var string $layout_path Location of layout
     */
    protected $layout_path;

    /**
     * @var bool $output Flag to check if the view been processed
     */
    protected $output = false;

    /**
     * @var bool $layout_output Flag to check if the layout has been processed
     */
    protected $layout_output = false;

    /**
     * @var string $view_content The resulting content after output (includes the layout as well)
     */
    protected $view_content = '';

    /**
     * Add the passed path to the $path variable and set the layout
     *
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path         The class to verify the set paths
     * @param string                                    $path         The location of the file to use
     * @param array                                     $paths        An array of file locations to check for, the first found path is used
     * @param string                                    $ext          The file extension to use
     * @param string                                    $layout_path  The location of the layout file to use
     * @param array                                     $layout_paths An array of layout file locations to check for, the first found path is used
     * @param string                                    $layout_ext   The layout file extension to use
     */
    public function __construct(PathInterface $Path, $path = '', $paths = [], $ext = '.php', $layout_path = '', $layout_paths = [], $layout_ext = '.php')
    {

        // Set the PathInterface
        $this->Path = $Path;

        // Check paths exist
        $this->path = $this->setPath($path, $paths, $ext);

        if (!empty($layout_path) || !empty($layout_paths)) {
            $this->setLayout($layout_path, $layout_paths, $layout_ext);
        }
    }

    /**
     * Build the path if it exists and return the resulting full path
     *
     * @param string $path  The location of the file to use
     * @param array  $paths An array of file locations to check for, the first found path is used
     * @param string $ext   The file extension to use
     *
     * @return string Return the checked path which will be used
     * @throws \Exception Error out if the path doesn't exist
     */
    private function buildPath($path = '', $paths = [], $ext = '.php')
    {

        /**
         * @var \Soapbox\Toolbox\Interfaces\PathInterface $path_check
         */
        $path_check = $this->Path->setPath($path)
                                 ->setPaths($paths)
                                 ->setExt($ext);

        if ($path_check->checkPaths()) {
            return $path_check->getRealPath();
        }

        throw new \Exception('Path does not exist: ' . $path_check->getPath() . ' within ' . implode(', ', $path_check->getPaths()));
    }

    /**
     * Set the path if it exists
     *
     * @param string $path  The location of the file to use
     * @param array  $paths An array of file locations to check for, the first found path is used
     * @param string $ext   The file extension to use
     *
     * @return string|null Returns the full path if it exists otherwise null
     */
    public function setPath($path = '', $paths = [], $ext = '.php')
    {

        if (!empty($paths)) {
            $path = $this->buildPath($path, $paths, $ext);
        } elseif (!empty($path)) {
            $path = $this->buildPath($path);
        }

        return $path;
    }

    /**
     * Set the layout if it exists
     *
     * @param string $layout_path  The location of the layout file to use
     * @param array  $layout_paths An array of layout file locations to check for, the first found path is used
     * @param string $layout_ext   The layout file extension to use
     */
    public function setLayout($layout_path = '', $layout_paths = [], $layout_ext = '.php')
    {

        if (!empty($layout_paths)) {
            $this->layout_path = $this->buildPath($layout_path, $layout_paths, $layout_ext);
        } else {
            $this->layout_path = $this->buildPath($layout_path);
        }
    }

    /**
     * Set the variables that need to be passed to the view
     *
     * @param array $params     An array of key => value pairs to pass to the view/layout
     * @param bool  $for_layout A flag to set whether these params are for the view or layout
     */
    public function setVars($params = [], $for_layout = false)
    {

        if ($for_layout) {
            $this->setLayoutVars($params);
        } else {

            if (!is_array($params)) {
                $params = [$params];
            }

            $this->passed_vars = array_merge($this->passed_vars, $params);
        }
    }

    /**
     * Set the variables that need to be passed to the layout
     *
     * @param array $params An array of key => value pairs to pass to the layout
     */
    public function setLayoutVars($params = [])
    {

        if (!is_array($params)) {
            $params = [$params];
        }

        $this->layout_vars = array_merge($this->layout_vars, $params);
    }

    /**
     * Update/create a single variable to pass to the view
     *
     * @param string $param      The key to use for the parameter
     * @param string $value      The value to use for the parameter
     * @param bool   $append     A flag to set whether to add to the end of an existing parameter or overwrite it
     * @param bool   $for_layout A flag to set whether to store the parameter for the view or the layout
     */
    public function setVar($param, $value = '', $append = true, $for_layout = false)
    {

        if ($for_layout) {
            $this->setLayoutVar($param, $value, $append);
        } else {

            if ($append && isset($this->passed_vars[$param])) {
                $value = $this->passed_vars[$param] . $value;
            }

            $this->passed_vars[$param] = $value;
        }
    }

    /**
     * Update/create a single variable to pass to the layout
     *
     * @param string $param  The key to use for the parameter
     * @param string $value  The value to use for the parameter
     * @param bool   $append A flag to set whether to add to the end of an existing parameter or overwrite it
     */
    public function setLayoutVar($param, $value = '', $append = true)
    {

        if ($append && isset($this->layout_vars[$param])) {
            $value = $this->layout_vars[$param] . $value;
        }

        $this->layout_vars[$param] = $value;
    }

    /**
     * Check to see if passed variables have been set
     *
     * @return bool Returns true/false if the variable is/is not set
     */
    public function hasVars()
    {

        return (!empty($this->passed_vars));
    }

    /**
     * Check to see if layout variables have been set
     *
     * @return bool Returns true/false if the variable is/is not set
     */
    public function hasLayoutVars()
    {

        return (!empty($this->layout_vars));
    }

    /**
     * Extract all the passed variables to individual PHP vars
     * Output buffer the include, run the include
     * And return the buffered results
     *
     * @param bool $multiple A flag to set whether this view is in a chain of views to display
     *
     * @throws \Exception Error out if the file to output isn't found
     * @return string Returns the resulting template string (normally HTML)
     */
    public function output($multiple = false)
    {

        // Convert passed values to PHP variables - http://php.net/manual/en/function.extract.php
        extract($this->passed_vars);

        /** @noinspection PhpUnusedLocalVariableInspection */
        // Store the variables for use in view, to pass them down the view chain (if needed)
        $view_passed_vars = $this->passed_vars;

        // Store a copy of the current View class to use within views rather than hard coding.
        if (!isset($View)) {
            $View = get_class($this);
        }

        if (!isset($Path)) {
            $Path = $this->Path;
        }

        ob_start();

        if (!empty($this->path)) {
            /** @noinspection PhpIncludeInspection */
            include $this->path;

            $this->output = true;

            if ($multiple) {
                $this->view_content .= ob_get_clean();
            } else {
                /*
                 * If the layout has been set, pass the view to the variable $view_content
                 * Extract the passed $_layout_vars and prefix with "layout_"
                 *
                 * Output buffer the include, run the include
                 * And return the buffered results
                 */
                if (!empty($this->layout_path)) {
                    $this->view_content .= ob_get_clean();
                    $this->layout_vars['view_content'] = $this->view_content;

                    // Convert passed values to PHP variables and prefix with `layout_` - http://php.net/manual/en/function.extract.php
                    extract($this->layout_vars, EXTR_PREFIX_ALL, 'layout');

                    ob_start();

                    /** @noinspection PhpIncludeInspection */
                    include $this->layout_path;

                    $this->layout_output = true;

                    return ob_get_clean();
                } else {
                    return ob_get_clean();
                }
            }
        }

        throw new \Exception('File could not be found: ' . $this->path);
    }

    /**
     * Check to see if an output has been generated
     *
     * @return bool Returns true/false if the output has/has not been created already
     */
    public function hasOutput()
    {

        return $this->output;
    }

    /**
     * Check to see if the layout has been generated
     *
     * @return bool Returns true/false if the layouts output has/has not been created already
     */
    public function hasLayoutOutput()
    {

        return $this->layout_output;
    }

    /**
     * Reset the class parameters to their default values
     * Keep the existing path if $keep_path
     *
     * @param array $keep Array of class variables to not revert to defaults
     *
     * @return View Return the reset instance of this class
     */
    public function reset(
        $keep = [
            'path',
            'layout_path'
        ]
    ) {

        foreach (get_class_vars(get_class($this)) as $var => $default_value) {
            if (in_array($var, $keep) === false) {
                $this->$var = $default_value;
            }
        }

        return $this;
    }
}
