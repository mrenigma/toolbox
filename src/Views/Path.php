<?php
namespace Soapbox\Toolbox\Views;

require_once __DIR__ . '/../../vendor/autoload.php';

use Soapbox\Toolbox\Interfaces\PathInterface;

/**
 * Class Path
 * A class to confirm and generate paths based upon the relative paths provided to it
 *
 * @package Soapbox\Toolbox\Views
 * @see     Soapbox\Toolbox\Interfaces\PathInterface
 */
class Path implements PathInterface
{

    /**
     * @var string $theme_path The location of the theme folder
     */
    protected $theme_path = '';

    /**
     * @var string $path The location of the file to use
     */
    protected $path = '';

    /**
     * @var array $paths An array of file locations to check for, the first found path is stored in $path
     */
    protected $paths = [];

    /**
     * @var string $ext The file extension to use
     */
    protected $ext = '';

    /**
     * Add the passed path to the $_path private variable.
     *
     * @param string $path       The location of the file to use
     * @param array  $paths      An array of file locations to check for, the first found path is used
     * @param string $ext        The file extension to use
     * @param string $theme_path The theme location if the default needs to be overridden
     */
    public function __construct($path = '', $paths = [], $ext = '.php', $theme_path = '')
    {

        if (!empty($paths)) {
            if (!is_array($paths)) {
                $paths = [$paths];
            }

            $this->paths = $paths;
        }

        $this->setExt($ext);

        $this->path = $path;

        $this->theme_path = $this->getThemePath($theme_path);
    }

    /**
     * Get the file extension
     *
     * @return string The extension saved in the class
     */
    public function getExt()
    {

        return $this->ext;
    }

    /**
     * Set the file extension
     *
     * @param string $ext The file extension to set
     *
     * @return $this Returns this class
     */
    public function setExt($ext)
    {

        if (!empty($ext)) {
            if (strpos($ext, '.') === false) {
                $ext = '.' . $ext;
            }

        }

        $this->ext = $ext;

        return $this;
    }

    /**
     * Get the path that was passed to this class
     *
     * @return string Return the stored path
     */
    public function getPath()
    {

        return $this->path;
    }

    /**
     * Set the path to the file location
     *
     * @param string $path The file location
     *
     * @return $this
     */
    public function setPath($path)
    {

        $this->path = $path;

        return $this;
    }

    /**
     * Get the paths that was passed to this class
     *
     * @return string Return the array of stored paths
     */
    public function getPaths()
    {

        return $this->paths;
    }

    /**
     * Set the paths to be checked within
     *
     * @param string|array $paths The locations where the $this->path may exist
     *
     * @return $this
     */
    public function setPaths($paths = [])
    {

        if (!is_array($paths)) {
            $paths = [$paths];
        }

        $this->paths = $paths;

        return $this;
    }

    /**
     * Get the full path
     *
     * @return string Return the full validated path
     */
    public function getRealPath()
    {

        // Get the full real path, converting traversal symbols
        return realpath($this->addDirectorySeparatorToEnd($this->theme_path) . $this->removeDuplicatePath($this->theme_path, $this->path));
    }

    /**
     * Check path exists and is within the themes folder
     *
     * @return bool Return if the path exists and is within the $theme_path
     */
    public function checkPath()
    {

        $this->path = $this->appendExtToPath();

        return ($this->getRealPath() && $this->withinThemes());
    }

    /**
     * Check path exists and is within the themes folder
     *
     * @return bool Return confirmation that a path within $paths was found and validated
     */
    public function checkPaths()
    {

        $found_path = '';
        $old_path   = $this->appendExtToPath($this->removeEndingDirectorySeparator());

        foreach ($this->paths as $path) {
            $path       = $this->removeEndingDirectorySeparator($path);
            $this->path = $this->addDirectorySeparatorToEnd($path) . $this->removeDuplicatePath($path, $old_path);

            if ($this->getRealPath() && $this->withinThemes()) {
                $found_path = $this->path;

                break;
            }
        }

        if (empty($found_path)) {
            $this->path = $old_path;
        }

        return ($this->getRealPath() && $this->withinThemes());
    }

    /**
     * Add extension to end of $path
     *
     * @param string $path The path to use, if not set the classes $path is used
     *
     * @return string Returns the path with the extension added
     */
    protected function appendExtToPath($path = '')
    {

        if (empty($path)) {
            $path = $this->path;
        }

        if (!empty($this->ext)) {
            if (mb_strpos($this->ext, '.') !== 0) {
                $this->ext = '.' . $this->ext;
            }

            $path = str_replace($this->ext, '', $path) . $this->ext;
        }

        return $path;
    }

    /**
     * Add directory separator to end of path
     *
     * @param string $path The path to use, if not set the classes $path is used
     *
     * @return string Return path with directory separator added to end
     */
    protected function addDirectorySeparatorToEnd($path = '')
    {

        if (empty($path)) {
            $path = $this->path;
        }

        $path_count = mb_strlen($path) - 1;

        if (mb_strrpos($path, DIRECTORY_SEPARATOR) !== $path_count) {
            $path .= DIRECTORY_SEPARATOR;
        }

        return $path;
    }

    /**
     * Removed directory separator from end of path
     *
     * @param string $path The path to use, if not set the classes $path is used
     *
     * @return string Return path with directory separator removed from end
     */
    protected function removeEndingDirectorySeparator($path = '')
    {

        if (empty($path)) {
            $path = $this->path;
        }

        $path_count = mb_strlen($path) - 1;

        if (mb_strrpos($path, DIRECTORY_SEPARATOR) === $path_count) {
            $path = mb_strcut($path, 0, $path_count);
        }

        return $path;
    }

    /**
     * Check for duplication of the path, if found, clean out.
     * This is used mainly for finding duplication of the preceding directory paths
     *
     * @param string $path    The path to search for
     * @param string $subject The path to search within
     *
     * @return string|bool Returns the trimmed path OR false if not found
     */
    protected function removeDuplicatePath($path, $subject)
    {

        if (substr_count($path, '..') || substr_count($path, '/') || substr_count($path, '\\')) {
            $path = realpath($path);
        }

        if (substr_count($subject, '..') || substr_count($subject, '/') || substr_count($subject, '\\')) {
            $backup_subject = $subject;
            $subject        = realpath($subject);

            if ($subject === false) {
                $subject = realpath($this->addDirectorySeparatorToEnd($path) . $backup_subject);
            }
        }

        return str_replace($this->addDirectorySeparatorToEnd($path), '', $subject);
    }

    /**
     * Get the themes path, checked with realpath()
     *
     * @param string $theme_path The path to use, if not set then use either WP or __DIR__ to get the location
     *
     * @return string Return real path of theme
     */
    public function getThemePath($theme_path = '')
    {

        /*
         * If you're using WordPress get the current themes folder
         * Otherwise get the parent of this classes folder
         * E.g. if /includes/views/ is the classes folder, it will return /includes/
        */
        if (empty($theme_path)) {
            if (!empty($this->theme_path)) {
                $theme_path = $this->theme_path;
            } else if (function_exists('get_stylesheet_directory')) {
                $theme_path = get_stylesheet_directory();
            } else {
                $theme_path = dirname(__DIR__);
            }
        }

        return realpath($theme_path);
    }

    /**
     * Check to see if the path contains the themes folder
     *
     * @return bool Return whether the path was contained within the theme folder
     */
    public function withinThemes()
    {

        return (mb_strpos($this->getRealPath(), $this->theme_path) !== false);
    }
} 
