<?php
namespace Soapbox\Toolbox\Interfaces;

/**
 * Interface ModelInterface
 *
 * @package Soapbox\Toolbox\Interfaces
 */
interface ModelInterface
{

    /**
     * Get the results of the query and return
     *
     * @param string $query The query string to process
     * @param mixed  $type  The type of output that should be returned
     *
     * @return mixed Return the results of the query
     */
    public function getResults($query, $type = null);

    /**
     * Check to see if the user is logged in or not
     *
     * @return bool True/False the user is logged in
     */
    public function isLoggedIn();

    /**
     * Check to see if the taxonomy is hierarchical or not based on the slug
     *
     * @param string $slug The alias of the taxonomy
     *
     * @return bool Returns whether the taxonomy is/is not hierarchical
     */
    public function isTaxonomyHierarchical($slug);

    /**
     * Get the users post statuses based on whether they are logged in or not
     *
     * @return string The post statuses for the user
     */
    public function getUserPostStatus();

    /**
     * Find all post types based on the $args, returned array values are based on the $output_type
     *
     * @param array  $args
     * @param string $output_type
     *
     * @return array Array of post types
     */
    public function getPostTypes(Array $args, $output_type = 'names');

    /**
     * Get all the posts
     *
     * @param array $args
     *
     * @return array Array of posts
     */
    public function getPosts(Array $args = []);

    /**
     * Get a list of categories based on the $args
     *
     * @param array $args
     *
     * @return array Array of categories
     */
    public function getCategories(Array $args = []);

    /**
     * Get a list of tags based on the $args
     *
     * @param array $args
     *
     * @return array Array of tags
     */
    public function getTags(Array $args = []);

    /**
     * Get the taxonomy by its slug
     *
     * @param string $slug Alias of taxonomy
     *
     * @return bool|object Returns the object if found otherwise it will return false
     */
    public function getTaxonomy($slug);

    /**
     * Get a list of terms for taxonomy
     *
     * @param array $taxonomies Array of taxonomies to get terms for
     * @param array $args       Array of arguments constraining the resulting array
     *
     * @return array Array of terms
     */
    public function getTerms(Array $taxonomies, Array $args = []);

    /**
     * Get a term by the $field, $value and $taxonomy
     *
     * @param string $field    The name of the field to find
     * @param string $value    The value of the field which the $field must equal
     * @param string $taxonomy The taxonomy the $field must reside in
     *
     * @return mixed Returns term or false if $taxonomy does not exist or the term was not found.
     */
    public function getTermBy($field = 'slug', $value = '', $taxonomy = 'post_tag');

    /**
     * Get the terms child elements
     *
     * @param int|string $term_id       The ID of the term
     * @param string     $taxonomy_slug The alias of the taxonomy to search against
     *
     * @return array|\WP_Error Returns an array of child elements OR an error
     */
    public function getTermChildren($term_id, $taxonomy_slug = '');

    /**
     * Get option by $name
     *
     * @param string $name    Name of the option
     * @param mixed  $default Default value if no value for the option is found
     *
     * @return mixed The value of the option if found otherwise the default value
     */
    public function getOption($name, $default = false);

    /**
     * Get custom terms based on the post status, post types and taxonomies
     *
     * @param string $post_status A comma delimited list of post statuses to check against
     * @param string $post_types  A comma delimited list of post types to check against
     * @param string $taxonomies  A comma delimited list of taxonomies to check against
     *
     * @return array Returns results from DB query
     */
    public function getCustomTerms($post_status, $post_types, $taxonomies);

    /**
     * @param array  $includes      Array of items to include in the query
     * @param array  $custom_fields Array of custom fields
     * @param string $slug          Taxonomy slug
     *
     * @return array Array of custom field ID's
     */
    public function getCustomFieldIds(Array $includes, Array $custom_fields, $slug);

    /**
     * return the common custom date is configured in options otherwise post_date
     *
     * @return string
     */
    public function getDateField();

    /**
     * @param array $order      Array of current order attributes
     * @param array $attributes New order attributes to merge into the $order array
     *
     * @return array New $order array
     */
    public function setOrderAttributes(Array $order, Array $attributes = []);

    /**
     * Set the default order attributes
     *
     * @return array Array of default order attributes
     */
    public function getDefaultOrder();

    /**
     * Get the correct order attributes based on $sort_by
     *
     * @param array   $order      Array of order attributes current set
     * @param  string $sort_by    The string to sort by
     * @param string  $date_field The date field to use
     *
     * @return array Array of new order attributes
     */
    public function getOrder(Array $order = [], $sort_by, $date_field = 'post_date');

    /**
     * Retrieve and cache the years of posts.
     *
     * @param               array   $exclude_types Types to be excluded from query
     * @param               boolean $force         Force the creation/update of the cache
     * @param boolean | int         $limit         Limit the number of years shown
     * @param               string  $field         The field name to get years from
     *
     * @return array Array of years
     */
    public function getYears(Array $exclude_types = [], $force = false, $limit = 0, $field = 'post_date');

    /**
     * Get a list of archive years set by archive and limit field
     *
     * @param     string $field date field
     * @param int        $max   limit to the maximum number of records
     *
     * @return array
     */
    public function getArchiveYears($field = 'default', $max = 5);

    /**
     * Set the posts where query
     *
     * @param array  $includes Array of items to include in the posts where
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return ModelInterface Returns the class in use
     */
    public function setPostWhere(Array $includes, $slug);

    /**
     * Get the query string for the meta key
     *
     * @param array  $includes Array of settings for the filters
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return string Post type query string
     */
    public function getMetaKeyQuery(Array $includes, $slug);

    /**
     * Get the query string for post types
     *
     * @param array  $includes Array of settings for the filters
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return string Post type query string
     */
    public function getPostTypesQuery(Array $includes, $slug);

    /**
     * Get the query string for filters
     *
     * @param array  $includes Array of settings for the filters
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return string Post type query string
     */
    public function getFiltersQuery(Array $includes, $slug);

    /**
     * Get the query string for categories
     *
     * @param array  $includes Array of settings for the filters
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return string Post type query string
     */
    public function getCategoriesQuery(Array $includes, $slug);

    /**
     * Generate year query based on the date fields
     *
     * @param                string $field         The field name to get years from
     * @param array                 $exclude_types Types to be excluded from query
     * @param  boolean | int        $limit         Limit the number of years shown
     *
     * @return string The MySQL query
     */
    public function getYearQuery($field, Array $exclude_types = [], $limit = 0);

    /**
     * Get the query string for dates
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getDateQuery(Array $includes, $slug, $post_status, Array $queries);

    /**
     * Get the query string for the date archive
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getDateArchiveQuery(Array $includes, $slug, $post_status, Array $queries);

    /**
     * Get the query string for the amount ranges
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getAmountRangeQuery(Array $includes, $slug, $post_status, Array $queries);

    /**
     * Get the query string for countries
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getCountryQuery(Array $includes, $slug, $post_status, Array $queries);

    /**
     * Get the query string for year differences
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getYearDiffQuery(Array $includes, $slug, $post_status, Array $queries);

    /**
     * Get the query string for an unspecified custom field
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getCustomFieldQuery(Array $includes, $slug, $post_status, Array $queries);
}
