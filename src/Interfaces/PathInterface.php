<?php
namespace Soapbox\Toolbox\Interfaces;

/**
 * Interface PathInterface
 *
 * @package Soapbox\Toolbox\Interfaces
 */
interface PathInterface
{

    /**
     * Get the file extension
     *
     * @return string The extension saved in the class
     */
    public function getExt();

    /**
     * Set the file extension
     *
     * @param string $ext The file extension to set
     *
     * @return $this Returns this class
     */
    public function setExt($ext);

    /**
     * Get the path that was passed to this class
     *
     * @return string Return the stored path
     */
    public function getPath();

    /**
     * Set the path to the file location
     *
     * @param string $path The file location
     *
     * @return $this
     */
    public function setPath($path);

    /**
     * Get the paths that was passed to this class
     *
     * @return string Return the array of stored paths
     */
    public function getPaths();

    /**
     * Set the paths to be checked within
     *
     * @param string|array $paths The locations where the $this->path may exist
     *
     * @return $this
     */
    public function setPaths($paths = []);

    /**
     * Get the full path
     *
     * @return string Return the full validated path
     */
    public function getRealPath();

    /**
     * Check path exists and is within the themes folder
     *
     * @return bool Return if the path exists and is within the $theme_path
     */
    public function checkPath();

    /**
     * Check path exists and is within the themes folder
     *
     * @return bool Return confirmation that a path within $paths was found and validated
     */
    public function checkPaths();

    /**
     * Get the themes path, checked with realpath()
     *
     * @param string $theme_path The path to use, if not set then use either WP or __DIR__ to get the location
     *
     * @return string Return real path of theme
     */
    public function getThemePath($theme_path = '');
} 
