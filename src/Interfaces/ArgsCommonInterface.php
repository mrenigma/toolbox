<?php
namespace Soapbox\Toolbox\Interfaces;

/**
 * Interface ArgsCommonInterface
 *
 * @package Soapbox\Toolbox\Interfaces
 */
interface ArgsCommonInterface
{

    /**
     * Get the passed variables
     *
     * @return array Return the array of variables stored in the class
     */
    public function getPassedVars();

    /**
     * Get a specific passed variable if it exists
     *
     * @param string $name The key for the variable
     *
     * @return mixed Return the value of the variable
     */
    public function getPassedVar($name);

    /**
     * Generate an array from the variables in the URL
     *
     * @param array $defaults An array of default variable values to use
     * @param bool  $reset    Reset the passed_variables array
     *
     * @return array Returns the array of processed variables
     */
    public function buildPassedVars(Array $defaults = [], $reset = false);

    /**
     * Add a custom variable to passed vars
     *
     * @param string $name  The key of the variable
     * @param mixed  $value The value of the variable
     *
     * @return array Array of passed variables after update
     */
    public function addPassedVar($name, $value = '');
}
