<?php
namespace Soapbox\Toolbox\Interfaces;

/**
 * Interface ArgsCommonInterface
 *
 * @package Soapbox\Toolbox\Interfaces
 */
interface ArgsBuilderInterface
{

    /**
     * Generate an array of arguments to be used with WP_Query
     *
     * @param array $includes Array of settings for the filters.
     *
     * @return array Array of WordPress $args
     */
    public function buildArgs(Array $includes);
}
