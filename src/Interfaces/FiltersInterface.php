<?php
namespace Soapbox\Toolbox\Interfaces;

/**
 * Interface FiltersInterface
 *
 * @package Soapbox\Toolbox\Interfaces
 */
interface FiltersInterface
{

    /**
     * Add a new path to the beginning of the $template_paths array
     *
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path The location of the files
     *
     * @return \Soapbox\Toolbox\Interfaces\FiltersInterface Returns itself
     */
    public function addPath($Path);

    /**
     *  buildFilters method to automatically generate a filters form.
     *
     * @param array  $filter_items   Array of filters to be shown.
     *                               Accepted values: 'category', 'post_type', 'tags', 'year', 'author'
     * @param array  $includes       Settings for the filters
     * @param string $extra_elements Any HTML elements to add to the end of the form but before the form end
     * @param string $pre_elements   Any HTML elements to add to the beginning of the form but after the form start
     *
     * @return string $html  HTML of the filter form that is outputted to the page
     */
    public function buildFilters(Array $filter_items, Array  $includes = [], $extra_elements = '', $pre_elements = '');

    /**
     * buildSorters method to automatically generate a sorting form.
     *
     * @return string HTML of the sorters form that is outputted to the page
     */
    public function buildSorters();

    /**
     * Clear the stored $html and the $rendered_vars array
     */
    public function clearHtml();
}
