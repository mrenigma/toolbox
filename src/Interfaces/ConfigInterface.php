<?php
namespace Soapbox\Toolbox\Interfaces;

/**
 * Interface ConfigInterface
 *
 * @package Soapbox\Toolbox\Interfaces
 */
interface ConfigInterface
{

    /**
     * Get the config file contents from the $_path
     * Process according to the type
     *
     * @param string $type Type of config file
     *
     * @return string
     */
    public function getConfig($type = 'ini');

    /**
     * Get the value based on the key.
     * You can use an associative array to retrieve deep values as needed
     *
     * @param string|array $key    The key of the value to find
     * @param array        $values The sub values to process if going deep
     *
     * @return string|null Return the value if found
     */
    public function get($key, $values = []);

    /**
     * Get all the values found in the config
     *
     * @return array
     */
    public function getAll();
}
