<?php
namespace Soapbox\Toolbox\Interfaces;

/**
 * Interface ViewInterface
 *
 * @package Soapbox\Toolbox\Interfaces
 */
interface ViewInterface
{

    /**
     * Set the path if it exists
     *
     * @param string $path  The location of the file to use
     * @param array  $paths An array of file locations to check for, the first found path is used
     * @param string $ext   The file extension to use
     *
     * @return string|null Returns the full path if it exists otherwise null
     */
    public function setPath($path = '', $paths = [], $ext = '.php');

    /**
     * Set the layout if it exists
     *
     * @param string $layout_path  The location of the layout file to use
     * @param array  $layout_paths An array of layout file locations to check for, the first found path is used
     * @param string $layout_ext   The layout file extension to use
     */
    public function setLayout($layout_path = '', $layout_paths = [], $layout_ext = '.php');

    /**
     * Set the variables that need to be passed to the view
     *
     * @param array $params     An array of key => value pairs to pass to the view/layout
     * @param bool  $for_layout A flag to set whether these params are for the view or layout
     */
    public function setVars($params = [], $for_layout = false);

    /**
     * Set the variables that need to be passed to the layout
     *
     * @param array $params An array of key => value pairs to pass to the layout
     */
    public function setLayoutVars($params = []);

    /**
     * Update/create a single variable to pass to the view
     *
     * @param string $param      The key to use for the parameter
     * @param string $value      The value to use for the parameter
     * @param bool   $append     A flag to set whether to add to the end of an existing parameter or overwrite it
     * @param bool   $for_layout A flag to set whether to store the parameter for the view or the layout
     */
    public function setVar($param, $value = '', $append = true, $for_layout = false);

    /**
     * Update/create a single variable to pass to the layout
     *
     * @param string $param  The key to use for the parameter
     * @param string $value  The value to use for the parameter
     * @param bool   $append A flag to set whether to add to the end of an existing parameter or overwrite it
     */
    public function setLayoutVar($param, $value = '', $append = true);

    /**
     * Check to see if passed variables have been set
     *
     * @return bool Returns true/false if the variable is/is not set
     */
    public function hasVars();

    /**
     * Check to see if layout variables have been set
     *
     * @return bool Returns true/false if the variable is/is not set
     */
    public function hasLayoutVars();

    /**
     * Extract all the passed variables to individual PHP vars
     * Output buffer the include, run the include
     * And return the buffered results
     *
     * @param bool $multiple A flag to set whether this view is in a chain of views to display
     *
     * @throws \Exception Error out if the file to output isn't found
     * @return string Returns the resulting template string (normally HTML)
     */
    public function output($multiple = false);

    /**
     * Check to see if an output has been generated
     *
     * @return bool Returns true/false if the output has/has not been created already
     */
    public function hasOutput();

    /**
     * Check to see if the layout has been generated
     *
     * @return bool Returns true/false if the layouts output has/has not been created already
     */
    public function hasLayoutOutput();

    /**
     * Reset the class parameters to their default values
     * Keep the existing path if $keep_path
     *
     * @param array $keep Array of class variables to not revert to defaults
     *
     * @return \Soapbox\Toolbox\Interfaces\ViewInterface Return the reset instance of this class
     */
    public function reset(
        $keep = [
            'path',
            'layout_path'
        ]
    );
}
