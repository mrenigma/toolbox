<?php
namespace Soapbox\Toolbox\Args;

use Soapbox\Toolbox\Interfaces\ArgsCommonInterface;

/**
 * Class ArgsCommon
 * A class to sanitize and process $_GET variables. To then be processed and used elsewhere
 *
 * @package Soapbox\Toolbox\Args
 * @see     Soapbox\Toolbox\Interfaces\ArgsCommonInterface
 */
class ArgsCommon implements ArgsCommonInterface
{

    /**
     * @var array $passed_variables Store years SQL Results if already queried
     */
    protected $passed_variables = [];

    /**
     * Build the passed variables using the defaults as a base
     *
     * @param array $defaults An array of default variable values to use
     */
    public function __construct(Array $defaults = [])
    {

        $this->passed_variables = $this->buildPassedVars($defaults);
    }

    /**
     * Get the passed variables
     *
     * @return array Return the array of variables stored in the class
     */
    public function getPassedVars()
    {

        return $this->passed_variables;
    }

    /**
     * Get a specific passed variable if it exists
     *
     * @param string $name The key for the variable
     *
     * @return mixed Return the value of the variable
     */
    public function getPassedVar($name)
    {

        if (!empty($name) && isset($this->passed_variables[$name])) {
            return $this->passed_variables[$name];
        }

        return null;
    }

    /**
     * Generate an array from the variables in the URL
     *
     * @param array $defaults An array of default variable values to use
     * @param bool  $reset    Reset the passed_variables array
     *
     * @return array Returns the array of processed variables
     */
    public function buildPassedVars(Array $defaults = [], $reset = false)
    {

        if (empty($this->passed_variables) || $reset) {
            $passed_vars = $defaults;
            $get_vars    = filter_input_array(INPUT_GET);

            if (!empty($get_vars)) {
                // Removing 'hidden-' and 'select-' from GET variable names
                // Creates an associative array of passed variables
                foreach ($get_vars as $key => $val) {
                    // Ignore Google Analytics Variables:
                    $ga_variables = [
                        'utm_source',
                        'utm_campaign',
                        'utm_medium',
                        'utm_term'
                    ];

                    if (strpos($key, 'hidden-') === false && !in_array($key, $ga_variables)) {
                        $key               = str_replace('select-', '', $key);
                        $passed_vars[$key] = $val;

                        unset($get_vars[$key]);
                    }
                }

                foreach ($get_vars as $key => $val) {
                    if (strpos($key, 'hidden-') !== false && !isset($passed_vars[str_replace('hidden-', '', $key)])) {
                        $key               = str_replace('hidden-', '', $key);
                        $passed_vars[$key] = $val;
                    }
                }
            }

            // Store as a private variable
            $this->passed_variables = $passed_vars;
        } else {
            // Pull from private variable
            $passed_vars = $this->passed_variables;
        }

        return $passed_vars;
    }

    /**
     * Add a custom variable to passed vars
     *
     * @param string $name  The key of the variable
     * @param mixed  $value The value of the variable
     *
     * @return array Array of passed variables after update
     */
    public function addPassedVar($name, $value = '')
    {

        if (!empty($name)) {
            $this->passed_variables[$name] = $value;
        }

        return $this->passed_variables;
    }
}
