<?php
namespace Soapbox\Toolbox\Args\ArgTypes;

use Soapbox\Toolbox\Interfaces\ArgTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;

class ArgPagination implements ArgTypesInterface
{

    /**
     * Build the arguments array based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param array                                      $includes         Array of settings for the filters
     * @param string                                     $slug             Settings key
     * @param mixed                                      $var              Settings value
     * @param array                                      $passed_variables Array of variable values
     *
     * @return mixed Array or value of filter argument(s)
     */
    public static function build(ModelInterface $Model, Array $includes, $slug, $var, Array $passed_variables = [])
    {

        // If custom $_GET variable name is set, use it.
// Default to 'current_page'
        if (isset($includes['pagination_id'])) {
            $paged_id = $includes['pagination_id'];
        } else {
            $paged_id = 'current_page';
        };

// Get current page value from the URL
        if (isset($passed_variables[$paged_id])) {
            $paged = $passed_variables[$paged_id];
        } else if (empty($paged)) {
            $paged = 1;
        }

// Reset back to page one, where applicable
        if (isset($passed_variables['reset-paged'])) {
            $paged = 1;
        }

        return $paged;
    }
}
