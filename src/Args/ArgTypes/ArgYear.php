<?php
namespace Soapbox\Toolbox\Args\ArgTypes;

use Soapbox\Toolbox\Interfaces\ArgTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;

class ArgYear implements ArgTypesInterface
{

    /**
     * Build the arguments array based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param array                                      $includes         Array of settings for the filters
     * @param string                                     $slug             Settings key
     * @param mixed                                      $var              Settings value
     * @param array                                      $passed_variables Array of variable values
     *
     * @return mixed Array or value of filter argument(s)
     */
    public static function build(ModelInterface $Model, Array $includes, $slug, $var, Array $passed_variables = [])
    {

        if (isset($includes['years_field']) && !empty($includes['years_field'])) {
            $years = $Model->getYears([], false, 0, $includes['years_field']);
        } else {
            $years = $Model->getYears();
        }

        $exclude = [];

        // Overwrite EXCLUDE array using the includes array
        if ($includes['years_include_or_exclude'] === 'include' && isset($includes['years'])) {
            foreach ($years as $year) {
                if (!in_array($year, $includes['years'])) {
                    $exclude[] = $year;
                }
            }
        }

        // Write EXCLUDE array using the includes array
        if ($includes['years_include_or_exclude'] === 'exclude') {
            $exclude = $includes['years'];
        }

        // Store dates from URL
        $date_arr = [];

        // Get dates from passed_vars
        if (is_array($var)) {
            foreach ($var as $i => $val) {

                // Include value if it is not in the exclude array
                if (!in_array($val, $exclude)) {
                    $date_arr[] = $val;
                }
            }
        } else {
            $date_arr[] = $var;
        }

        //get the common date field
        $dateField = $Model->getDateField();

        $args = [];

        // Iterate through $date_arr array and add each year as a date_query
        foreach ($date_arr as $year) {

            //if date field is default post_date run the date query otherwise custom meta query
            if ($dateField === 'post_date') {
                if ($year !== 'Archive') {
                    $args['date_query'][] = [
                        'year' => $year
                    ];
                } else {
                    $archive_years = $Model->getArchiveYears('default', intval($includes['years_limit']));

                    if (!empty($archive_years)) {
                        foreach ($archive_years as $archive_year) {
                            $args['date_query'][] = [
                                'year' => $archive_year
                            ];
                        }
                    }
                }
            } else {
                $args['meta_query'][] = [
                    'compare' => 'REGEXP',
                    'key'     => $dateField,
                    'value'   => '^' . $year
                ];
            }
        }

        return $args;
    }
}
