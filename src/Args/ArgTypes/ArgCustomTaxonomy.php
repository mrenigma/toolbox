<?php
namespace Soapbox\Toolbox\Args\ArgTypes;

use Soapbox\Toolbox\Interfaces\ArgTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;

class ArgCustomTaxonomy implements ArgTypesInterface
{

    /**
     * Build the arguments array based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param array                                      $includes         Array of settings for the filters
     * @param string                                     $slug             Settings key
     * @param mixed                                      $var              Settings value
     * @param array                                      $passed_variables Array of variable values
     *
     * @return mixed Array or value of filter argument(s)
     */
    public static function build(ModelInterface $Model, Array $includes, $slug, $var, Array $passed_variables = [])
    {

        $taxonomies = [
            $slug
        ];

        $exclude = [];

        $args = [
            'orderby'           => 'name',
            'order'             => 'ASC',
            'hide_empty'        => true,
            'exclude'           => [],
            'exclude_tree'      => [],
            'include'           => [],
            'number'            => '',
            'fields'            => 'all',
            'slug'              => '',
            'parent'            => '',
            'hierarchical'      => true,
            'child_of'          => 0,
            'get'               => '',
            'name__like'        => '',
            'description__like' => '',
            'pad_counts'        => false,
            'offset'            => '',
            'search'            => '',
            'cache_domain'      => 'core'
        ];

        $terms      = $Model->getTerms($taxonomies, $args);
        $term_names = [];

        if (!empty($terms)) {
            foreach ($terms as $term) {
                // Check if tag has posts
                if ($term->count > 0) {
                    $term_names[] = $term->slug;
                }
            }
        }

        $include_exclude_key = $slug . '_include_or_exclude';

        $key = $slug . '_terms';

        // Overwrite EXCLUDE array using the includes array
        if (!empty($includes[$include_exclude_key]) && $includes[$include_exclude_key] === 'include' && isset($includes[$key])) {
            foreach ($term_names as $term_name) {
                if (!in_array($term_name, $includes[$key])) {
                    $exclude[] = $term_name;
                }
            }
        }

        // Write EXCLUDE array using the includes array
        if (!empty($includes[$include_exclude_key]) && $includes[$include_exclude_key] === 'exclude' && !empty($includes[$key])) {
            $exclude = $includes[$key];
        }

        $filtered_terms = [];

        if (empty($exclude)) {
            $exclude = [];
        }

        if (is_array($var)) {
            foreach ($var as $i => $val) {
                if (!in_array($val, $exclude)) {
                    $filtered_terms[] = $val;
                }
            }
        } else {
            $filtered_terms = $var;
        }

        $args = [];

        if (!empty($filtered_terms)) {
            $operator = 'IN';

            if (isset($includes[$slug . '_operator'])) {
                // Possible values:
                // AND
                // IN
                // NOT IN
                $operator = $includes[$slug . '_operator'];
            }

            $args['tax_query'][] = [
                'taxonomy' => $slug,
                'field'    => 'slug',
                'terms'    => $filtered_terms,
                'operator' => $operator
            ];
        }

        return $args;
    }
}
