<?php
namespace Soapbox\Toolbox\Args\ArgTypes;

use Soapbox\Toolbox\Interfaces\ArgTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;

class ArgCategory implements ArgTypesInterface
{

    /**
     * Build the arguments array based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param array                                  $includes         Array of settings for the filters
     * @param string                                 $slug             Settings key
     * @param mixed                                  $var              Settings value
     * @param array                                  $passed_variables Array of variable values
     *
     * @return mixed Array or value of filter argument(s)
     */
    public static function build(ModelInterface $Model, Array $includes, $slug, $var, Array $passed_variables = [])
    {

        $category_args = [
            'taxonomy'   => 'category',
            'orderby'    => 'menu_order',
            'hide_empty' => 1
        ];

        $categories = $Model->getCategories($category_args);
        $cats       = [];

        foreach ($categories as $cat) {
            $cats[$cat->slug] = $cat->term_id;
        }

        // Overwrite EXCLUDE array using the includes array
        if ($includes['categories_include_or_exclude'] === 'include' && isset($includes['categories'])) {
            foreach ($cats as $cat => $cat_id) {
                if (!in_array($cat, $includes['categories'])) {
                    $exclude[] = $cat;
                }
            }
        }

        // Write EXCLUDE array using the includes array
        if ($includes['categories_include_or_exclude'] === 'exclude') {
            $exclude = $includes['categories'];
        }

        if (empty($exclude)) {
            $exclude = [];
        }

        $var_array = [];

        if (is_array($var) && !empty($var)) {
            foreach ($var as $i => $val) {
                if (!in_array($val, $exclude) && isset($cats[$val])) {
                    $var_array[] = $cats[$val];
                }
            }
        } else if (!empty($var)) {
            $var_array[] = $var;
        }

        $operator = 'category__in';

        if (isset($includes['category_operator'])) {
            // Possible values:
            // category__and (array) - use category id.
            // category__in (array) - use category id.
            // category__not_in (array) - use category id.
            $operator = $includes['category_operator'];
        }

        return [$operator => $var_array];
    }
}
