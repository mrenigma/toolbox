<?php
namespace Soapbox\Toolbox\Args\ArgTypes;

use Soapbox\Toolbox\Interfaces\ArgTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;

class ArgPostTag implements ArgTypesInterface
{

    /**
     * Build the arguments array based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param array                                      $includes         Array of settings for the filters
     * @param string                                     $slug             Settings key
     * @param mixed                                      $var              Settings value
     * @param array                                      $passed_variables Array of variable values
     *
     * @return mixed Array or value of filter argument(s)
     */
    public static function build(ModelInterface $Model, Array $includes, $slug, $var, Array $passed_variables = [])
    {

        $tag_args = [
            'taxonomy'   => 'post_tag',
            'orderby'    => 'menu_order',
            'hide_empty' => 1
        ];

        $tags[]    = $Model->getTags($tag_args);
        $tag_names = [];
        $exclude   = [];

        foreach ($tags[0] as $tag) {
            // Check if tag has posts
            if ($tag->count > 0) {
                $tag_names[] = $tag->slug;
            }
        }

        // Overwrite EXCLUDE array using the includes array
        if ($includes['tags_include_or_exclude'] === 'include' && isset($includes['tags'])) {
            foreach ($tag_names as $tag_name) {
                if (!in_array($tag_name, $includes['tags'])) {
                    $exclude[] = $tag_name;
                }
            }
        }

        // Write EXCLUDE array using the includes array
        if ($includes['tags_include_or_exclude'] === 'exclude') {
            $exclude = $includes['tags'];
        }

        $args = [];

        if (is_array($var) && !empty($var)) {
            foreach ($var as $i => $val) {
                if (!in_array($val, $exclude)) {
                    $tag               = $Model->getTermBy('slug', $val, 'post_tag');
                    $args['tag__in'][] = $tag->term_id;
                }
            }
        } else if (!empty($var)) {
            $tag               = $Model->getTermBy('slug', $var, 'post_tag');
            $args['tag__in'][] = $tag->term_id;
        }

        return $args;
    }
}
