<?php
namespace Soapbox\Toolbox\Args\ArgTypes;

use Soapbox\Toolbox\Interfaces\ArgTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;

class ArgOrderBy implements ArgTypesInterface
{

    /**
     * Get the correct order from the Model
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model
     * @param array                                      $order            Current order arguments
     * @param array                                      $passed_variables Array of variable values
     *
     * @return array Returns array of new order arguments
     */
    protected static function switchOrderBy(ModelInterface $Model, $order = [], Array $passed_variables = [])
    {

        return $Model->getOrder($order, $passed_variables['sort-by'], $Model->getDateField());
    }

    /**
     * Build the arguments array based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param array                                  $includes         Array of settings for the filters
     * @param string                                 $slug             Settings key
     * @param mixed                                  $var              Settings value
     * @param array                                  $passed_variables Array of variable values
     *
     * @return mixed Array or value of filter argument(s)
     */
    public static function build(ModelInterface $Model, Array $includes, $slug, $var, Array $passed_variables = [])
    {

        $order = $Model->getDefaultOrder();

// If there is a sort value - sort the query
        if (isset($passed_variables['sort-by'])) {
            $order = self::switchOrderBy($Model, $order, $passed_variables);
        }

        return $order;
    }
}