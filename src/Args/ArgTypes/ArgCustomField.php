<?php
namespace Soapbox\Toolbox\Args\ArgTypes;

use Soapbox\Toolbox\Interfaces\ArgTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;

class ArgCustomField implements ArgTypesInterface
{

    /**
     * @param       string $slug         Settings key
     * @param       string $custom_field Custom field value
     * @param array        $includes     Array of settings for the filters
     *
     * @return array Array of arguments to process an date slider
     */
    private static function getDateSliderArgs($slug, $custom_field, Array $includes)
    {

        $val      = explode('lt', $custom_field);
        $start_up = $val[1];
        $val      = explode('gt', $val[0]);

        $start_up  = substr($start_up, 0, 4) . '-' . substr($start_up, 4);
        $start_low = $val[1];
        $start_low = substr($start_low, 0, 4) . '-' . substr($start_low, 4);

        $low_num = intval(substr($start_low, 0, 4));
        $up_num  = intval(substr($start_up, 5));

        if ($up_num < 80) {
            $up_num += 2000;
        } else {
            $up_num += 1900;
        }

        $suffix = intval(substr($start_low, 5));

        $year_values = [];

        for ($j = $low_num; $j < $up_num; $j++) {
            $year = $j . '-';

            if ($suffix < 10) {
                $year .= '0';
            }

            $year .= $suffix;

            $year_values[] = $year;
            $suffix++;
        }

        return [
            'compare' => 'IN',
            'key'     => $includes[$slug . '_meta_key'],
            'value'   => $year_values
        ];
    }

    /**
     * @param       string $slug         Settings key
     * @param       string $custom_field Custom field value
     * @param array        $includes     Array of settings for the filters
     *
     * @return array Array of arguments to process an amount slider
     */
    private static function getAmountSliderArgs($slug, $custom_field, Array $includes)
    {

        //without commas on values
        if (stripos($custom_field, 'gt') !== false && stripos($custom_field, 'lt') === false) {
            $max_amount = substr($custom_field, 2);
            $max_amount = str_replace('k', '000', $max_amount);

            return [
                'compare' => '>',
                'key'     => $includes[$slug . '_meta_key'],
                'value'   => $max_amount,
                'type'    => 'numeric'
            ];
        } //with commas and £ on values
        else {
            if (stripos($custom_field, 'lt') !== false && stripos($custom_field, 'gt') === false) {
                $max_amount = substr($custom_field, 2);
                $max_amount = str_replace('k', '000', $max_amount);
                $min_amount = 0;
            }

            if (stripos($custom_field, 'lt') !== false && stripos($custom_field, 'gt') !== false) {
                $custom_field = str_replace('gt', '', $custom_field);

                $min_amount = substr($custom_field, 0, stripos($custom_field, 'lt'));
                $max_amount = str_replace($min_amount . 'lt', '', $custom_field);

                $min_amount = str_replace('k', '000', $min_amount);
                $max_amount = str_replace('k', '000', $max_amount);
            }

            if (!isset($min_amount)) {
                $min_amount = 0;
            }

            if (!isset($max_amount)) {
                $max_amount = 1000000;
            }

            return [
                'compare' => 'BETWEEN',
                'key'     => $includes[$slug . '_meta_key'],
                'value'   => [
                    $min_amount,
                    $max_amount
                ],
                'type'    => 'numeric'
            ];
        }
    }

    /**
     * Build the arguments array based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param array                                      $includes         Array of settings for the filters
     * @param string                                     $slug             Settings key
     * @param mixed                                      $var              Settings value
     * @param array                                      $passed_variables Array of variable values
     *
     * @return mixed Array or value of filter argument(s)
     */
    public static function build(ModelInterface $Model, Array $includes, $slug, $var, Array $passed_variables = [])
    {

        $custom_field_array = [];

        if (is_array($var)) {
            foreach ($var as $i => $val) {
                $custom_field_array[] = $val;
            }
        } else {
            $custom_field_array[] = $var;
        }

        $Model->setPostWhere($includes, $slug);

        // Iterate through $var_array and add each year as a tax_query
        $custom_field_array = $Model->getCustomFieldIds($includes, $custom_field_array, $slug);

        if (empty($includes[$slug . '_compare'])) {
            $includes[$slug . '_compare'] = '=';
        }

        $args = [];

        if (!empty($custom_field_array) && !empty($custom_field_array[0])) {
            foreach ($custom_field_array as $custom_field) {
                if (isset($includes[$slug . '_meta_key'])) {
                    //date sliders
                    if (isset($includes[$slug . '_input_type']) && $includes[$slug . '_input_type'] === 'slider' && isset($includes[$slug . '_field_type']) && $includes[$slug . '_field_type'] === 'date') {
                        $args['meta_query'][] = self::getDateSliderArgs($slug, $custom_field, $includes);
                    } //amount sliders
                    elseif (isset($includes[$slug . '_input_type']) && $includes[$slug . '_input_type'] === 'slider' && isset($includes[$slug . '_field_type']) && $includes[$slug . '_field_type'] === 'amount_range') {
                        $args['meta_query'][] = self::getAmountSliderArgs($slug, $custom_field, $includes);
                    } else {
                        $args['meta_query'][] = [
                            'compare' => $includes[$slug . '_compare'],
                            'key'     => $includes[$slug . '_meta_key'],
                            'value'   => $custom_field
                        ];
                    }
                }
            }
        }

        return $args;
    }
}
