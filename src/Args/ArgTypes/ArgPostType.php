<?php
namespace Soapbox\Toolbox\Args\ArgTypes;

use Soapbox\Toolbox\Interfaces\ArgTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;

class ArgPostType implements ArgTypesInterface
{

    /**
     * Get the default post type arguments
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param array                                      $includes         Array of settings for the filters
     * @param string                                     $slug             Settings key
     * @param mixed                                      $var              Settings value
     * @param array                                      $passed_variables Array of variable values
     *
     * @return array Return post_type
     */
    public static function defaultPostTypeArgs(ModelInterface $Model, Array $includes, $slug, $var, Array $passed_variables = [])
    {

        $post_type = '';

        // What to do if post_type not set
        if (!isset($passed_variables['post_type']) && $includes['types']) {
            if ($includes['types_include_or_exclude'] === 'exclude') {
                $types_to_show = [];

                // Args for get_post_types()
                $post_type_args = [
                    'public' => true,
                ];

                $post_types = $Model->getPostTypes($post_type_args, 'name');

                foreach ($post_types as $post_name) {
                    if (!in_array($post_name->name, $includes['types'])) {
                        $types_to_show[] = $post_name->name;
                    }
                }

                $post_type = $types_to_show;

            } else if ($includes['types_include_or_exclude'] === 'include') {
                $post_type = $includes['types'];
            }
        } else {
            $post_type = $passed_variables['post_type'];
        }

        return $post_type;
    }

    /**
     * Build the arguments array based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param array                                      $includes         Array of settings for the filters
     * @param string                                     $slug             Settings key
     * @param mixed                                      $var              Settings value
     * @param array                                      $passed_variables Array of variable values
     *
     * @return mixed Array or value of filter argument(s)
     */
    public static function build(ModelInterface $Model, Array $includes, $slug, $var, Array $passed_variables = [])
    {

        // Args for get_post_types()
        $post_type_args = [
            'public' => true,
        ];

        $post_types = $Model->getPostTypes($post_type_args, 'name');

        $types_to_exclude = [];

        if ($includes['types_include_or_exclude'] === 'exclude') {

            foreach ($post_types as $post_name) {
                if (in_array($post_name->name, $includes['types'])) {
                    $types_to_exclude[] = $post_name->name;
                }
            }

        } else if ($includes['types_include_or_exclude'] === 'include') {
            foreach ($post_types as $post_name) {
                if (is_object($post_name) && !in_array($post_name->name, $includes['types'])) {
                    $types_to_exclude[] = $post_name->name;
                }
            }
        }

        $args = [];

        if (is_array($var) && !empty($var)) {
            foreach ($var as $i => $val) {
                if (!in_array($val, $types_to_exclude)) {

                    $args_post_type = [
                        'name' => $val
                    ];

                    $type = $Model->getPostTypes($args_post_type);

                    $type_name = $type[$val];

                    $args['post_type'][] = $type_name;
                }
            }
        }

        return $args;
    }
}
