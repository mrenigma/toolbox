<?php
namespace Soapbox\Toolbox\Args;

use Soapbox\Toolbox\Interfaces\ArgsBuilderInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;
use Soapbox\Toolbox\Interfaces\PathInterface;

/**
 * Class ArgsBuilder
 * A class to generate the arguments used by the CMS/framework.
 * It normally takes arguments passed from Soapbox\Toolbox\Args\ArgsCommonInterface
 *
 * @package Soapbox\Toolbox\Args
 * @see     Soapbox\Toolbox\Interfaces\ArgsBuilderInterface
 * @see     Soapbox\Toolbox\Interfaces\ArgsCommonInterface
 */
class ArgsBuilder implements ArgsBuilderInterface
{

    /**
     * @var \Soapbox\Toolbox\Interfaces\ModelInterface $Model The model to get data from
     */
    protected $Model;

    /**
     * @var \Soapbox\Toolbox\Interfaces\PathInterface $Path The class to verify the set paths
     */
    protected $Path;

    /**
     * @var array $passed_variables Store years SQL Results if already queried
     */
    public $passed_variables;

    /**
     * @var array $args Store for $args array
     */
    public $args = [];

    /**
     * @var array $excluded_vars Array of variables to exclude from the build process
     */
    public $excluded_vars = [
        's',
        'paged',
        'current-page',
        'sort-by',
        'lang'
    ];

    /**
     * @var array $extra_arg_types Array of class name => path pairs to check over for potential argument types
     */
    public $extra_arg_types = [];

    /**
     * Set the class dependencies
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model           The DB model to retrieve data from
     * @param \Soapbox\Toolbox\Interfaces\PathInterface  $Path            The class to verify the set paths
     * @param array                                      $passed_vars     Array of variable values built in Soapbox\Toolbox\Args\ArgsCommon
     * @param array                                      $extra_arg_types Array of class name => path pairs to dynamically add new argument types
     */
    public function __construct(ModelInterface $Model, PathInterface $Path, Array $passed_vars = [], Array $extra_arg_types = [])
    {

        // Store the model
        $this->Model = $Model;
        // Store the path
        $this->Path = $Path;

        // Retrieve and store passed vars array
        $this->passed_variables = $passed_vars;

        // Store the new argument types
        $this->extra_arg_types = $this->validateExtraArgTypes($extra_arg_types);
    }

    /**
     * Get the class name which this class understands
     *
     * @param string $slug The base class name
     *
     * @return string The class name that works with this class
     */
    private function getClassName($slug)
    {

        if ($slug === 'categories') {
            $slug = 'category';
        } else if ($slug === 'tag') {
            $slug = 'post_tag';
        }

        return 'Arg' . str_replace(' ', '', ucwords(str_replace('_', ' ', str_replace('-', ' ', $slug))));
    }

    /**
     * Check the extra arg types to make sure they exist, if not remove them from the array
     *
     * @param array $extra_arg_types
     *
     * @return array Returns validated extra arg types
     */
    private function validateExtraArgTypes(Array $extra_arg_types)
    {

        foreach ($extra_arg_types as $class => $path) {
            $path_check = $this->Path->setPath($path);

            if ($path_check->checkPath()) {
                $extra_arg_types[$class] = $path_check->getRealPath();
            } else {
                unset($extra_arg_types[$class]);
            }
        }

        return $extra_arg_types;
    }

    /**
     * Check to see if the arg type exists in the $extra_arg_types array
     *
     * @param string $class_name The class name ot search for
     *
     * @return bool Returns true/false if the class exists/does not exist
     */
    private function extraArgTypeExists($class_name)
    {

        if (!empty($this->extra_arg_types[$class_name])) {
            return $this->extra_arg_types[$class_name];
        }

        return false;
    }

    /**
     * Get the class to process the argument
     *
     * @param string $slug The base slug of the class
     *
     * @return bool|string Returns the FQN for the class
     */
    private function getArgClass($slug)
    {

        $name       = $this->getClassName($slug);
        $class_name = $this->extraArgTypeExists($name);

        if (!empty($class_name)) {
            /** @noinspection PhpIncludeInspection */
            include_once $class_name;

            $class_name = $name;
        } else {
            $class_name = $this->argTypeExists($name);
        }

        return $class_name;
    }

    /**
     * Check to see if the arg type exists in the autoloaded classes
     *
     * @param string $class_name The class name to search for
     *
     * @return bool|string Returns the class FQN or false if no class exists
     */
    protected function argTypeExists($class_name)
    {

        $name   = __NAMESPACE__ . '\ArgTypes\\' . $class_name;
        $exists = class_exists($name);

        if ($exists) {
            return $name;
        }

        return false;
    }

    /**
     * Set the arg type value(s)
     *
     * @param string  $class    The FQN of the class
     * @param array   $includes Array of settings for the filters
     * @param string  $slug     The slug of the arg type to process
     * @param  string $var      The value of the arg type to process
     * @param string  $method   The method to use within the FQN class
     *
     * @return array Returns the new arg type value(s) array or an empty array on failure
     */
    protected function setArgType($class, Array $includes, $slug, $var, $method = 'build')
    {

        if (!empty($class)) {
            /** @noinspection PhpUndefinedMethodInspection */
            return $class::{$method}($this->Model, $includes, $slug, $var, $this->passed_variables);
        }

        return [];
    }

    /**
     * Check for query logic, if none exists then add it to the arguments
     *
     * @param $args Array of arguments
     *
     * @return array Array of updated arguments
     */
    protected function checkQueryLogic($args)
    {

        $query_logic = [
            'date_query' => 'OR',
            'meta_query' => 'OR',
            'tax_query'  => 'AND'
        ];

        foreach ($query_logic as $query => $logic) {
            if (isset($args[$query]) && !isset($args[$query]['relation'])) {
                $args[$query]['relation'] = $logic;
            }
        }

        return $args;
    }

    /**
     * @param array $includes Array of settings for the filters
     *
     * @return array Array of built arguments
     */
    protected function argsSwitch(Array $includes)
    {

        $built_args = [];

        foreach ($this->passed_variables as $k => $var) {
            if (!in_array($k, $this->excluded_vars)) {
                $args       = [];
                $class_name = $this->getArgClass($k);

                // If it's a defined ArgType
                if (!empty($class_name)) {
                    $args = $this->setArgType($class_name, $includes, $k, $var);
                } else {
                    // Try to do it as a Custom Field
                    if (isset($includes[$k . '_type']) && $includes[$k . '_type'] === 'customfield') {
                        $args = $this->setArgType($this->getArgClass('custom_field'), $includes, $k, $var);
                    } // Fallback to Custom Taxonomy
                    else if (isset($includes[$k . '_include_or_exclude'])) {
                        $args = $this->setArgType($this->getArgClass('custom_taxonomy'), $includes, $k, $var);
                    }
                }

                // Merge the new arguments into the existing array
                $built_args = array_merge_recursive($built_args, $args);

                // Make sure the query logic is added as needed!
                $built_args = $this->checkQueryLogic($built_args);
            }
        }

        // Return the built arguments
        return $built_args;
    }

    /**
     * Generate an array of arguments to be used with WP_Query
     *
     * @param array $includes Array of settings for the filters.
     *
     * @return array Array of WordPress $args
     */
    public function buildArgs(Array $includes)
    {

        // Retain search term.
        if (isset($this->passed_variables['s'])) {
            $this->args['s'] = $this->passed_variables['s'];
        };

        //if user is logged in private and public posts are available
        if ($this->Model->isLoggedIn()) {
            $this->args = array_merge($this->args, $this->setArgType($this->getArgClass('post_status'), $includes, 'post_status', [
                'publish',
                'private'
            ]));
        } else {
            // Only show published content
            $this->args = array_merge($this->args, $this->setArgType($this->getArgClass('post_status'), $includes, 'post_status', 'publish'));
        };

        // Set amount of posts per page.
        $this->args['posts_per_page'] = $this->setArgType($this->getArgClass('post_per_page'), $includes, 'posts_per_page', null);

        // Set args 'order' and 'orderby'
        $this->args = array_merge($this->args, $this->setArgType($this->getArgClass('order_by'), $includes, 'orderby', null));

        // Set args pagination
        $this->args['paged'] = $this->setArgType($this->getArgClass('pagination'), $includes, 'paged', null);

        // Default Post Type Functionality
        $this->args['post_type'] = $this->setArgType($this->getArgClass('post_type'), $includes, 'post_type', null, 'defaultPostTypeArgs');

        /* Iterate through $this->_passed_variables ($_GET Values)
         * Constructs $args using the values in passed_vars
         */
        if (isset($this->passed_variables)) {
            $this->args = array_merge($this->args, $this->argsSwitch($includes));
        }

        return $this->args;
    }
}