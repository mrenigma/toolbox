<aside class="sorting">
    <span class="sort-by">Sort by:</span>

    <form id="sort-form" method="get">
        <select class="sorter" name="sort-by">
            <?php
            if (isset($get_vars['sort-by'])) {
                $sort_by = $get_vars['sort-by'];
            } else {
                $sort_by = '';
            } ?>

            <?php
            if ($sort_by === "ndate") {
                $selected = '  selected="selected"  ';
            } else {
                $selected = ' ';
            }

            echo '<option value="ndate" ' . $selected . '>Date - Newest</option>';

            if ($sort_by === "odate") {
                $selected = '  selected="selected"  ';
            } else {
                $selected = ' ';
            }

            echo '<option value="odate" ' . $selected . '>Date - Oldest</option>';

            if ($sort_by === "titleaz") {
                $selected = '  selected="selected"  ';
            } else {
                $selected = ' ';
            }

            echo '<option value="titleaz" ' . $selected . '>Title A-Z</option>';

            if ($sort_by === "titleza") {
                $selected = '  selected="selected"  ';
            } else {
                $selected = ' ';
            }

            echo '<option value="titleza" ' . $selected . '>Title Z-A</option>';
            ?>
        </select>

        <?php
        if (isset($get_vars)) {
            foreach ($get_vars as $k => $var) {
                if ($k !== 'paged' && $k !== 'sort-by' && $k !== 's') {
                    if (is_array($var)) {
                        foreach ($var as $i => $val) {
                            if (!in_array($val, $exclude)) {
                                echo '<input type="hidden" name="hidden-' . $k . '[]" value="' . $val . '">';
                            }
                        }
                    } else {
                        echo '<input type="hidden" name="hidden-' . $k . '[]" value="' . $var . '">';
                    }
                } else if ($k === 's') {
                    echo '<input type="hidden" name="' . $k . '" value="' . $var . '">';
                }
            }
        }
        ?>
        <input class="sorter-submit" type="submit" value="sort">
    </form>
</aside>
