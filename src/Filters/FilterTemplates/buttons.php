<?php
use Soapbox\Toolbox\Filters\FilterTypes\FilterType;

if (empty($includes)) {
    $includes = [];
}

if (isset($buttons)) { ?>
    <div class="c-filters__buttons">
        <?php
        if (in_array('submit', $buttons)) {
            $apply_text = FilterType::getIniString($includes, 'apply_text', 'Apply filters');
            ?>
            <input class="c-filters__button c-filters__button--submit" type="submit" value="<?php echo $apply_text ?>">
            <?php
        }

        if (in_array('reset', $buttons)) {
            $reset_text = FilterType::getIniString($includes, 'reset_text', 'Clear filters');
            ?>
            <input class="c-filters__button c-filters__button--clear" type="reset" value="<?php echo $reset_text; ?>">
            <?php
        } ?>
    </div>
    <?php
}
