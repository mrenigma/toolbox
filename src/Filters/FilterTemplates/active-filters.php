<?php
use Soapbox\Toolbox\Filters\FilterTypes\FilterType;

$div_class = ' no-title';

if (empty($exclude)) {
    $exclude = [];
}

if (empty($title)) {
    $title = '';
}

if (empty($custom_children)) {
    $custom_children = [];
}

if (!empty($filters)) {
    // Loop through the returned filters
    foreach ($filters as $filter) {
        if (!in_array($filter->slug, $exclude)) {
            if (!isset($select_slug)) {
                $select_slug = preg_replace('/\s/', '_', strtolower($title));
            }

            if (isset($select_prefix)) {
                //$select_slug = $select_prefix . $select_slug;
            }

            if (isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) {
                $checked = ' checked="checked"';
            } else {
                $checked = '';
            }

            // Check if current taxonomy is hierarchical
            switch ($select_slug) {
                case 'categories':
                    $hierarchical_check_slug = 'category';
                    break;

                default:
                    $hierarchical_check_slug = $select_slug;
                    break;
            }

            $is_hierarchical = $Model->isTaxonomyHierarchical($hierarchical_check_slug);

            // Get IDs of child terms of current taxonomy
            if ($is_hierarchical) {
                $children = $Model->getTermChildren($filter->term_id, $hierarchical_check_slug);
            }

            $custom_children = FilterType::getFilterCustomChildren($includes, $filter->slug);

            $filter_display_name = FilterType::getFilterOptionLabel($includes, $filter->slug, $filter->name);

            if ((intval($filter_display_name) === 0 || !empty($filter_display_name)) && !empty($checked)) {
                ?>
                <div class="active-filter <?php echo $filter->slug; ?>">
                    <?php if (!empty($select_all)) { ?>
                        <span class="toggle-parent-category">Toggle</span>
                        <span class="label parent-category" data-for="<?php echo $filter->slug; ?>"><?php echo strtoupper($filter_display_name); ?></span>
                    <?php } else { ?>
                        <span class="checkbox-icon"></span>
                        <input class="checkbox" type="checkbox" name="select-<?php echo $select_slug; ?>[]" value="<?php echo $filter->slug; ?>" <?php echo $checked; ?> id="<?php echo $filter->slug; ?>">
                        <label class="label" for="<?php echo $filter->slug; ?>"><?php echo $filter_display_name; ?></label>
                        <?php
                    } ?>
                </div>
                <?php
            }
            // Output child term list
            if (!empty($children) && $is_hierarchical) {
                foreach ($children as $child_id) {
                    $child_term = $Model->getTermBy('id', $child_id, $hierarchical_check_slug);

                    if ((isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) || (isset($get_vars[$select_slug]) && in_array($child_term->slug,
                                $get_vars[$select_slug]))
                    ) {
                        $checked = ' checked="checked"';
                    } else {
                        $checked = '';
                    }

                    if (!in_array($child_term->slug, $exclude) && !empty($checked)) {
                        $child_display_name = FilterType::getFilterOptionLabel($includes, $child_term->slug, $child_term->name);
                        ?>
                        <div>
                            <input class="checkbox" type="checkbox" name="select-<?php echo $select_slug; ?>[]" value="<?php echo $child_term->slug; ?>" <?php echo $checked; ?> id="<?php echo $child_term->slug; ?>">
                            <span class="checkbox-icon"></span>
                            <label class="label" for="<?php echo $child_term->slug; ?>"><?php echo $child_display_name; ?></label>
                        </div>
                        <?php
                    }
                }

                if (!empty($select_all)) {
                    if (isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) {
                        $checked = ' checked="checked"';
                    } else {
                        $checked = '';
                    } ?>
                    <div>
                        <input class="checkbox select_all" type="checkbox" name="select-<?php echo $select_slug; ?>[]" value="<?php echo $filter->slug; ?>" <?php echo $checked; ?> id="select_all_<?php echo $filter->slug; ?>">
                        <span class="checkbox-icon"></span>
                        <label class="label" for="select_all_<?php echo $filter->slug; ?>">Select All</label>
                    </div>
                    <?php
                }

            }
            // Custom children set in .ini file.
            if (!empty($custom_children)) {
                if (!is_array($custom_children)) {
                    $custom_children = [$custom_children];
                }

                foreach ($custom_children as $slug => $name) {
                    $children_select_slug = FilterType::getChildrenFilterSlug($includes, $filter->slug, $select_slug);

                    if (isset($get_vars[$children_select_slug]) && in_array($slug, $get_vars[$children_select_slug])) {
                        $checked = ' checked="checked"';
                    } else {
                        $checked = '';
                    }

                    $custom_child_display_name = FilterType::getFilterOptionLabel($includes, $slug, $name);
                    ?>
                    <div>
                        <span class="checkbox-icon"></span>
                        <input class="checkbox" type="checkbox" name="select-<?php echo $children_select_slug; ?>[]" value="<?php echo $slug; ?>" <?php echo $checked; ?> id="<?php echo $slug; ?>">
                        <label class="label" for="<?php echo $slug; ?>"><?php echo $custom_child_display_name; ?></label>
                    </div>
                    <?php
                }
            }
        }
    }
}
