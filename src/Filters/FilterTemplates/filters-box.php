<?php

if (!empty($filters)) {
    // Title Class
    if (empty($title)) {
        $div_class = ' no-title';
    } else {
        $div_class = '';
    }

    // Filter Class
    if (empty($filter_class)) {
        $filter_class = ' default';
    }

    // Input type fallback
    if (empty($input_type)) {
        $input_type = 'checkbox';
    }
    ?>
    <div class="c-filters c-filters--box <?php echo $div_class, ' ', $filter_class, '-container'; ?>">
        <?php
        if (!empty($title)) {
            ?>
            <h2 class="c-filters__heading">
                <?php echo $title ?><span class="icon"></span>
            </h2>
            <?php
        }
        ?>
        <ul class="c-filters__list c-filters__list--<?php echo $filter_class; ?>" <?php echo !empty($get_vars) && array_key_exists($filter_class, $get_vars) ? '' : 'style="display:none"' ?>>
            <?php
            $view_name = 'filter-element-' . $input_type;
            $view      = new $View(new $Path(), get_toolbox_directory() . '/Filters/FilterTemplates/FormElements/' . $view_name . '.php');

            $view->setVars($view_passed_vars);
            $view->setVar('is_box', true);

            echo $view->output();
            ?>
        </ul>
    </div>
    <?php
}
