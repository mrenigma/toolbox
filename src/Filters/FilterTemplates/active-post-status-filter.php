<?php
use Soapbox\Toolbox\Filters\FilterTypes\FilterType;

if (empty($title)) {
    $div_class = ' no-title';
} else {
    $div_class = '';
}

if (!empty($get_vars) && !empty($get_vars[$select_slug]) && !empty($get_vars[$select_slug][0])) {
    if (!empty($filters)) {
        foreach ($filters as $filter) {
            $filter_display_name = FilterType::getFilterOptionLabel($includes, $filter['slug'], $filter['name']);
            ?>
            <div class="active-filter <?php echo $filter['slug']; ?>">
                <label class="label" for="<?php echo $filter['slug']; ?>"><?php echo $filter_display_name; ?></label>

                <span data-id="<?php echo $filter['slug']; ?>" data-name="<?php echo $select_slug; ?>" class="remove-filter">Remove</span>
            </div>
            <?php
        }
    }
} ?>

