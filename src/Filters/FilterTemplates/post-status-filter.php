<?php
use Soapbox\Toolbox\Filters\FilterTypes\FilterType;

if (empty($title)) {
    $div_class = ' no-title';
    $title     = '';
} else {
    $div_class = '';
}

if (!empty($filters)) {
    ?>
    <div class="filters<?php echo $div_class, ' ', $filter_class, '-container'; ?>">
        <?php
        if (!empty($title)) {
            ?>
            <h2 class="sidebar-heading">
                <?php echo $title ?><span class="icon"></span>
            </h2>
            <?php
        }
        ?>
        <ul class="filter-menu <?php echo $filter_class; ?>-filter">
            <?php
            foreach ($filters as $filter) {
                if (!in_array($filter['slug'], $exclude)) {
                    if (!isset($select_slug)) {
                        $select_slug = preg_replace('/\s/', '_', strtolower($title));
                    }

                    if (isset($select_prefix)) {
                        //$select_slug = $select_prefix . $select_slug;
                    }

                    if (isset($get_vars[$select_slug]) && in_array($filter['slug'], $get_vars[$select_slug])) {
                        $checked = ' checked="checked"';
                    } else {
                        $checked = '';
                    }
                    $filter_display_name = FilterType::getFilterOptionLabel($includes, $filter['slug'], $filter['name']);

                    if (!empty($filter_display_name)) {
                        ?>
                        <li class="<?php echo $filter['slug']; ?>">
                            <input class="checkbox" type="checkbox" name="select-<?php echo $select_slug; ?>[]" value="<?php echo $filter['slug']; ?>" <?php echo $checked; ?> id="<?php echo $filter['slug']; ?>">
                            <label class="label" for="<?php echo $filter['slug']; ?>"><?php echo $filter_display_name; ?></label>
                        </li>
                        <?php
                    }
                }
            }
            ?>
        </ul>
    </div>
    <?php
}
?>

