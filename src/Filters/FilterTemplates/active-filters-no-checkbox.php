<?php
use Soapbox\Toolbox\Filters\FilterTypes\FilterType;

$div_class = ' no-title';

if (empty($title)) {
    $title = '';
}

if (!empty($filters)) {

    //if input type is not set Ex: not slider, button
    if (empty($input_type)) {

        // Loop through the returned filters
        foreach ($filters as $filter) {

            // Check to see if is to be excluded
            if (!in_array($filter->slug, $exclude)) {

                // Select Slug fallback
                if (!isset($select_slug)) {
                    $select_slug = preg_replace('/\s/', '_', strtolower($title));
                }

                if (isset($includes[$select_slug . '_suffix'])) {
                    $suffix = $includes[$select_slug . '_suffix'];
                } else {
                    $suffix = '';
                }

                if (isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) {
                    $checked       = ' checked="checked"';
                    $checked_class = 'is-checked';
                } else {
                    $checked       = '';
                    $checked_class = '';
                }

                // Check if current taxonomy is hierarchical
                switch ($select_slug) {
                    case 'categories':
                        $hierarchical_check_slug = 'category';
                        break;

                    default:
                        $hierarchical_check_slug = $select_slug;
                        break;
                }

                $is_hierarchical = $Model->isTaxonomyHierarchical($hierarchical_check_slug);

                // Get IDs of child terms of current taxonomy
                if ($is_hierarchical) {
                    $children = $Model->getTermChildren($filter->term_id, $hierarchical_check_slug);
                }

                // Check the config file for any custom child options or custom option labels
                if (!empty($includes)) {
                    // Custom children
                    $custom_children = FilterType::getFilterCustomChildren($includes, $filter->slug);

                    // Custom labels
                    $filter_display_name = FilterType::getFilterOptionLabel($includes, $filter->slug, $filter->name);
                }

                if ((intval($filter_display_name) === 0 || !empty($filter_display_name)) && !empty($checked)) {

                    $view = new $View(new $Path(), get_toolbox_directory() . '/Filters/FilterTemplates/FormElements/active-filter.php');
                    $view->setVars($view_passed_vars);
                    $view->setVars([
                        'slug'                => $filter->slug,
                        'filter_display_name' => $filter_display_name,
                        'select_slug'         => $select_slug,
                        'suffix'              => $suffix,
                        'Model'               => $Model
                    ]);

                    echo $view->output();

                }
                // Output child term list
                if (!empty($children) && $is_hierarchical) {
                    foreach ($children as $child_id) {
                        $child_term = $Model->getTermBy('id', $child_id, $hierarchical_check_slug);

                        if ((isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) || (isset($get_vars[$select_slug]) && in_array($child_term->slug,
                                    $get_vars[$select_slug]))
                        ) {
                            $checked       = ' checked="checked"';
                            $checked_class = ' is-checked';
                        } else {
                            $checked       = '';
                            $checked_class = '';
                        }

                        if (!in_array($child_term->slug, $exclude) && !empty($checked)) {
                            $child_display_name = FilterType::getFilterOptionLabel($includes, $child_term->slug, $child_term->name);

                            $view = new $View(new $Path(), get_toolbox_directory() . '/Filters/FilterTemplates/FormElements/active-filter.php');
                            $view->setVars($view_passed_vars);
                            $view->setVars([
                                'slug'                => $child_term->slug,
                                'filter_display_name' => $child_display_name,
                                'select_slug'         => $select_slug,
                                'suffix'              => $suffix,
                                'Model'               => $Model
                            ]);

                            echo $view->output();
                        }
                    }

                    if (!empty($select_all)) {
                        if (isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) {
                            $checked = ' checked="checked"';
                        } else {
                            $checked = '';
                        }

                        $view = new $View(new $Path(), get_toolbox_directory() . '/Filters/FilterTemplates/FormElements/active-filter.php');
                        $view->setVars($view_passed_vars);
                        $view->setVars([
                            'slug'                => $filter->slug,
                            'filter_display_name' => 'Select All',
                            'select_slug'         => $select_slug,
                            'suffix'              => '',
                            'Model'               => $Model
                        ]);

                        echo $view->output();
                    }

                }
                // Custom children set in .ini file.
                if (!empty($custom_children)) {
                    foreach ($custom_children as $slug => $name) {
                        $children_select_slug = FilterType::getChildrenFilterSlug($includes, $filter->slug, $select_slug);

                        if (isset($get_vars[$children_select_slug]) && in_array($slug, $get_vars[$children_select_slug])) {
                            $checked = ' checked="checked"';
                        } else {
                            $checked = '';
                        }

                        $custom_child_display_name = FilterType::getFilterOptionLabel($includes, $slug, $name);

                        $view = new $View(new $Path(), get_toolbox_directory() . '/Filters/FilterTemplates/FormElements/active-filter.php');
                        $view->setVars($view_passed_vars);
                        $view->setVars([
                            'slug'                => $slug,
                            'filter_display_name' => $custom_child_display_name,
                            'select_slug'         => $children_select_slug,
                            'suffix'              => $suffix,
                            'Model'               => $Model
                        ]);

                        echo $view->output();
                    }
                }
            }
        }
    } //sliders, buttons, etc
    elseif (!empty($get_vars) && $input_type === 'slider') { //slider
        if (!empty($get_vars[$select_slug]) && !empty($get_vars[$select_slug][0])) {
            $val = $get_vars[$select_slug][0];

            $val       = explode('lt', $val);
            $start_up  = $val[1];
            $val       = explode('gt', $val[0]);
            $start_low = $val[1];

            if (isset($includes[$select_slug . '_prefix'])) {
                $currency = $includes[$select_slug . '_prefix'];
            } else {
                $currency = '';
            }

            if (isset($includes[$select_slug . '_suffix'])) {
                $suffix = $includes[$select_slug . '_suffix'];
            } else {
                $suffix = '';
            }

            $value = $currency . $start_low;

            if ($start_low !== $start_up) {

                $value .= '&nbsp;–&nbsp;' . $start_up;
            }
            $value .= $suffix;

            $view = new $View(new $Path(), get_toolbox_directory() . '/Filters/FilterTemplates/FormElements/active-filter.php');
            $view->setVars($view_passed_vars);
            $view->setVars([
                'slug'                => $select_slug,
                'filter_display_name' => $value,
                'select_slug'         => $select_slug,
                'suffix'              => '',
                'Model'               => $Model
            ]);

            echo $view->output();
        }
    }
}
