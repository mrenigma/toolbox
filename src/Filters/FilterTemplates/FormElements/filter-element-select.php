<?php
use Soapbox\Toolbox\Filters\FilterTypes\FilterType;

if (empty($title)) {
    $title = '';
}

if (!empty($filters)) {
    ?>
    <li class="c-filters__list__item c-filters__list__item--select">
        <select class="c-filters__list__item__select" name="select-<?php echo $select_slug; ?>[]">
            <?php
            // Loop through the returned filters
            foreach ($filters as $filter) {

                // Check to see if is to be excluded
                if (!in_array($filter->slug, $exclude)) {

                    // Fallback select_slug
                    if (!isset($select_slug)) {
                        $select_slug = preg_replace('/\s/', '_', strtolower($title));
                    }

                    // Set selected value
                    if (isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) {
                        $checked = ' selected="selected"';
                    } else {
                        $checked = '';
                    }

                    // Check the config file for any custom child options or custom option labels
                    if (!empty($includes)) {
                        // Custom labels
                        if (isset($filter->filter_name)) {
                            $filter_name = $filter->filter_name;
                        } else {
                            $filter_name = $filter->name;
                        }

                        $filter_display_name = FilterType::getFilterOptionLabel($includes, $filter->slug, $filter_name);
                    }

                    if (!empty($filter_display_name)) {
                        //  HTML Comment stops a spacing issue
                        ?>
                        <option
                            class="c-filters__list__item__select__option c-filters__list__item__select__option--<?php echo $filter->slug; ?>"
                            id="<?php echo $filter->slug; ?>"
                            value="<?php echo $filter->slug; ?>"
                            <?php echo $checked; ?>><!--
                        --><?php echo $filter_display_name; ?><!--
                    --></option>
                        <?php
                    }
                }
            } ?>
        </select>
    </li>
    <?php
} ?>
