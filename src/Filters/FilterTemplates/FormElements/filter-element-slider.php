<?php
if (isset($filters[0]->filter_name)) {
    $filter_name_0 = $filters[0]->filter_name;
} else {
    $filter_name_0 = $filters[0]->name;
}

if (isset($filters[1]->filter_name)) {
    $filter_name_1 = $filters[1]->filter_name;
} else {
    $filter_name_1 = $filters[1]->name;
}

if (isset($get_vars[$select_slug]) && !empty($get_vars[$select_slug][0])) {
    $val       = $get_vars[$select_slug][0];
    $val       = explode('lt', $val);
    $start_up  = $val[1];
    $val       = explode('gt', $val[0]);
    $start_low = $val[1];
    $active    = ' active';
} else {
    $start_low = trim($filter_name_0);
    $start_up  = trim($filter_name_1);
    $active    = '';
}

if (isset($get_vars[$select_slug])) {
    $final_value = $get_vars[$select_slug][0];
} else {
    $final_value = '';
}
?>

<li class="c-filters__list__item c-filters__list__item--slider c-slider">

    <div class="c-slider__scale c-filters__list__item__slider-scale js-slider <?php echo $filter_class, $active; ?>"
         id="<?php echo $filter_class ?>">
    </div>

    <div class="c-slider__values c-filters__list__item__slider-values">
        <span class="c-slider__values__lower"><?php echo $start_low ?></span><!--
        -->&nbsp;–&nbsp;<!--
        --><span class="c-slider__values__upper"><?php echo $start_up ?></span>

        <input type="hidden"
               class="c-filters__list__item__hidden-input js-final-value"
               name="select-<?php echo $select_slug; ?>[]"
               value="<?php echo $final_value; ?>"/>

        <input type="hidden"
               class="c-filters__list__item__hidden-input js-min-value"
               value="<?php echo str_replace(' ', '', $filter_name_0) ?>"/>

        <input type="hidden"
               class="c-filters__list__item__hidden-input js-max-value"
               value="<?php echo str_replace(' ', '', $filter_name_1) ?>"/>
    </div>

</li>
