<?php
if (empty($slug)) {
    $slug = '';
}

if (empty($filter_display_name)) {
    $filter_display_name = '';
}

if (empty($suffix)) {
    $suffix = '';
}

if (empty($select_slug)) {
    $select_slug = '';
}
?>
<div class="c-active-filter c-active-filter--<?php echo $slug; ?>">
    <label class="c-active-filter__label"
           for="<?php echo $slug; ?>">
        <?php echo $filter_display_name . $suffix; ?>
    </label>

    <span class="c-active-filter__remove js-remove-filter"
          data-id="<?php echo $slug; ?>"
          data-name="<?php echo $select_slug; ?>">
        Remove
    </span>
</div>
