<?php
foreach ($filters as $filter) {
    if (isset($get_vars[$select_slug])) {
        $val = $get_vars[$select_slug][0];
    } else {
        $val = $filters[0]->slug;
    }

    if (!empty($val) && $val === $filter->slug) {
        $active_class = ' is-active';
    } else {
        $active_class = '';
    };

    if (isset($filter->filter_name)) {
        $filter_name = $filter->filter_name;
    } else {
        $filter_name = $filter->name;
    }

    ?>
    <li class="c-filters__list__item c-filters__list__item--button">
        <button class="c-filters__list__item--button__button <?php echo $active_class; ?>"
                value="<?php echo $filter->slug; ?>">
            <?php echo strtoupper($filter_name); ?>
        </button>

        <?php
        if (!empty($get_vars[$select_slug])) {
            $button_value = $get_vars[$select_slug][0];
        } else {
            $button_value = '';
        }
        ?>
        <input class="c-filters__list__item__hidden-input"
               id="js-button-value"
               type="hidden"
               value="<?php echo $button_value; ?>"
               name="select-<?php echo $select_slug; ?>[]">
    </li>
    <?php
}
