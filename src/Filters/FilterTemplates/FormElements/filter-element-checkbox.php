<?php
use Soapbox\Toolbox\Filters\FilterTypes\FilterType;

if (empty($is_box)) {
    $is_box = false;
}

if (empty($filters)) {
    $filters = [];
}

$current_filter_count = 0;
$total_filter_count   = count($filters);

if ($is_box) {
    // Loop through the returned filters
    $number_of_rows = ceil($total_filter_count / 2);
} else {
    $number_of_rows = 0;
}

if (empty($filters['errors'])) {

// Loop through the returned filters
    foreach ($filters as $filter) {

        if ($is_box) {
            if (($current_filter_count % $number_of_rows) === 0) {
                echo '<ul class="filter-columns">';
            }
        }

        // Check to see if is to be excluded
        if (!in_array($filter->slug, $exclude)) {

            // Select Slug fallback
            if (!isset($select_slug) && !empty($title)) {
                $select_slug = preg_replace('/\s/', '_', strtolower($title));
            }

            // Set checked value
            if (isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) {
                $checked       = ' checked="checked"';
                $checked_class = 'is-checked';
            } else {
                $checked       = '';
                $checked_class = '';
            }

            // Check if current taxonomy is hierarchical
            if ($select_slug === 'categories') {
                $hierarchical_check_slug = 'category';
            } else {
                $hierarchical_check_slug = $select_slug;
            }

            $is_hierarchical = $Model->isTaxonomyHierarchical($hierarchical_check_slug);

            // Get IDs of child terms of current taxonomy
            $children_class = '';

            if ($is_hierarchical) {
                $children = $Model->getTermChildren($filter->term_id, $hierarchical_check_slug);

                if (!empty($children)) {
                    $children_class = 'has-children';
                }
            }

            // Check the config file for any custom child options or custom option labels
            if (!empty($includes)) {
                // Custom children
                $custom_children = FilterType::getFilterCustomChildren($includes, $filter->slug);

                if (isset($filter->filter_name)) {
                    $filter_name = $filter->filter_name;
                } else {
                    $filter_name = $filter->name;
                }

                // Custom labels
                $filter_display_name = FilterType::getFilterOptionLabel($includes, $filter->slug, $filter_name);
            }

            if (intval($filter_display_name, 10) === 0 || !empty($filter_display_name)) {
                ?>
                <li class="c-filters__list__item c-filters__list__item--checkbox <?php echo $filter->slug, ' ', $children_class, ' ', $checked_class; ?>">
                    <?php
                    if (!empty($select_all)) {
                        ?>
                        <span class="toggle-parent-category">
                        Toggle
                    </span>

                        <span class="label parent-category"
                              data-for="<?php echo $filter->slug; ?>">
                        <?php echo $filter_display_name; ?>
                    </span>
                        <?php
                    } else {
                        ?>
                        <input class="checkbox"
                               id="<?php echo $filter->slug; ?>"
                               name="select-<?php echo $select_slug; ?>[]"
                               type="checkbox"
                               value="<?php echo $filter->slug; ?>"
                            <?php echo $checked; ?>/>
                        <label class="label"
                               for="<?php echo $filter->slug; ?>">
                            <?php echo $filter_display_name; ?>
                        </label>
                        <?php
                    }

                    // Output child term list
                    if (!empty($children) && $is_hierarchical) {
                        ?>
                        <ul class="child-terms">
                            <?php
                            foreach ($children as $child_id) {
                                $child_term = $Model->getTermBy('id', $child_id, $hierarchical_check_slug);

                                if ((isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) || (isset($get_vars[$select_slug]) && in_array($child_term->slug,
                                            $get_vars[$select_slug]))
                                ) {
                                    $checked       = ' checked="checked"';
                                    $checked_class = ' is-checked';
                                } else {
                                    $checked       = '';
                                    $checked_class = '';
                                }

                                if (!in_array($child_term->slug, $exclude)) {
                                    $child_display_name = FilterType::getFilterOptionLabel($includes, $child_term->slug, $child_term->name);
                                    ?>
                                    <li class="<?php echo $checked_class; ?>">
                                        <input class="checkbox"
                                               id="<?php echo $child_term->slug; ?>"
                                               type="checkbox"
                                               name="select-<?php echo $select_slug; ?>[]"
                                               value="<?php echo $child_term->slug; ?>"
                                            <?php echo $checked; ?> />
                                        <label class="label"
                                               for="<?php echo $child_term->slug; ?>">
                                            <?php echo $child_display_name; ?>
                                        </label>
                                    </li>
                                    <?php
                                }
                            }

                            if (!empty($select_all)) {
                                if (isset($get_vars[$select_slug]) && in_array($filter->slug, $get_vars[$select_slug])) {
                                    $checked = ' checked="checked"';
                                } else {
                                    $checked = '';
                                } ?>
                                <li>
                                    <input class="checkbox select_all"
                                           id="select_all_<?php echo $filter->slug; ?>"
                                           type="checkbox"
                                           name="select-<?php echo $select_slug; ?>[]"
                                           value="<?php echo $filter->slug; ?>"
                                        <?php echo $checked; ?>
                                    />
                                    <label class="label" for="select_all_<?php echo $filter->slug; ?>">
                                        Select All
                                    </label>
                                </li>
                                <?php
                            } ?>
                        </ul>
                        <?php
                    }

                    // Custom children set in .ini file.
                    if (!empty($custom_children)) {
                        ?>
                        <ul class="child-terms">
                            <?php
                            foreach ($custom_children as $slug => $name) {

                                $children_select_slug = FilterType::getChildrenFilterSlug($includes, $filter->slug, $select_slug);

                                if (isset($get_vars[$children_select_slug]) && in_array($slug, $get_vars[$children_select_slug])) {
                                    $checked = ' checked="checked"';
                                } else {
                                    $checked = '';
                                }

                                $custom_child_display_name = FilterType::getFilterOptionLabel($includes, $slug, $name);
                                ?>
                                <li>
                                    <input class="checkbox"
                                           id="<?php echo $slug; ?>"
                                           type="checkbox"
                                           name="select-<?php echo $children_select_slug; ?>[]"
                                           value="<?php echo $slug; ?>"
                                        <?php echo $checked; ?> />
                                    <label class="label"
                                           for="<?php echo $slug; ?>">
                                        <?php echo $custom_child_display_name; ?>
                                    </label>
                                </li>
                                <?php
                            } ?>
                        </ul>
                        <?php
                    } ?>
                </li>
                <?php
            }
        }

        $current_filter_count++;

        // Filters-box view
        if ($is_box) {
            if (($current_filter_count % $number_of_rows) === 0 || $total_filter_count === $current_filter_count) {
                echo '</ul>';
            }
        }
    }
}