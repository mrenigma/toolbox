<?php
use Soapbox\Toolbox\Filters\FilterTypes\FilterType;

$submit_location = '';

if (empty($includes)) {
    $includes = [];
}

if (empty($form_elements)) {
    $form_elements = '';
}

$ini_submit_location = FilterType::getIniString($includes, 'submit_location');

if (!empty($ini_submit_location)) {
    $submit_location = $ini_submit_location;
}
?>
<form class="filter-form" method="get" action="<?php echo $submit_location; ?>">
    <?php echo $form_elements; ?>
</form>
