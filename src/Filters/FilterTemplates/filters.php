<?php

if (!empty($filters)) {
    // Title class
    if (empty($title)) {
        $div_class = ' no-title';
    } else {
        $div_class = '';
    }

    // Filter Class
    if (empty($filter_class)) {
        $filter_class = ' default';
    }

    // Input type fallback
    if (empty($input_type)) {
        $input_type = 'checkbox';
    }

    if (empty($view_passed_vars)) {
        $view_passed_vars = [];
    }
    ?>
    <div class="c-filters <?php echo $div_class, ' ', $filter_class, '-container'; ?>">
        <?php
        if (!empty($title)) {
            ?>
            <h2 class="c-filters__heading">
                <?php echo $title ?><span class="icon"></span>
            </h2>
            <?php
        }
        ?>
        <ul class="c-filters__list c-filters__list--<?php echo $filter_class; ?>">
            <?php
            $view_name = 'filter-element-' . $input_type;
            $view      = new $View(new $Path(), get_toolbox_directory() . '/Filters/FilterTemplates/FormElements/' . $view_name . '.php');

            $view->setVars($view_passed_vars);

            echo $view->output();
            ?>
        </ul>
    </div>
    <?php
}
