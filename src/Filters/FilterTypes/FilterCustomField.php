<?php
namespace Soapbox\Toolbox\Filters\FilterTypes;

use Soapbox\Toolbox\Interfaces\FilterTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;
use Soapbox\Toolbox\Interfaces\ViewInterface;

class FilterCustomField extends FilterType implements FilterTypesInterface
{

    /**
     * Build the filter View based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface  $View             The view to display the data
     * @param array                                      $includes         Array of settings for the filters
     * @param array                                      $rendered_vars    Array of variables which have been rendered already
     * @param array                                      $passed_variables Array of variable values
     * @param string                                     $slug             The alias of the filter
     * @param string                                     $field_type       The type of field to check against
     *
     * @return object Returns the resulting View output and the rendered variables sub-array
     *                This must return an object with has a minimum of these two properties: html, rendered_vars
     */
    public static function build(ModelInterface $Model, ViewInterface $View, Array $includes, Array $rendered_vars, Array $passed_variables, $slug, $field_type = '')
    {

        $html = '';

        $post_type_query = $Model->getPostTypesQuery($includes, $slug);

        if (isset($includes[$slug . '_meta_key'])) {
            $meta_key_query = $Model->getMetaKeyQuery($includes, $slug);
            $filter_query   = $Model->getFiltersQuery($includes, $slug);
            $category_query = $Model->getCategoriesQuery($includes, $slug);

            $post_status = $Model->getUserPostStatus();
            $queries     = compact('post_type_query', 'meta_key_query', 'filter_query', 'category_query');
            $method      = self::getQueryMethodName($field_type);

            if (method_exists($Model, $method)) {
                $sql_query = $Model->{$method}($includes, $slug, $post_status, $queries);
            } else {
                $sql_query = $Model->getCustomFieldQuery($includes, $slug, $post_status, $queries);
            }

            if (!empty($sql_query)) {
                $custom_fields = $Model->getResults($sql_query);
            } else {
                $custom_fields = [];
            }

            if (!empty($limit) && ($limit + 1) !== count($custom_fields)) {
                unset($custom_fields[count($custom_fields) - 1]);
            }

            // Overwrite EXCLUDE array using the includes array
            if (isset($includes[$slug]) && isset($includes[$slug . '_include_or_exclude'])) {
                if ($includes[$slug . '_include_or_exclude'] === 'include') {
                    foreach ($custom_fields as $custom_field) {
                        if (!in_array($custom_field->slug, $includes[$slug])) {
                            $exclude[] = $custom_field->slug;
                        }
                    }
                }
            }

            // Write EXCLUDE array using the includes array
            if (isset($includes[$slug]) && isset($includes[$slug . '_include_or_exclude'])) {
                if ($includes[$slug . '_include_or_exclude'] === 'exclude') {

                    $exclude = $includes[$slug];
                }
            }

            if (empty($exclude)) {
                $exclude = [];
            }

            if (!isset($includes[$slug . '_input_type'])) {
                $includes[$slug . '_input_type'] = '';
            }

            $View->setVars([
                'exclude'      => $exclude,
                'filters'      => $custom_fields,
                'filter_class' => $slug,
                'get_vars'     => $passed_variables,
                'includes'     => $includes,
                'select_slug'  => $slug,
                'title'        => self::getFilterLabel($includes, $slug, ucwords(str_replace('_', ' ', str_replace('-', ' ', $slug)))),
                'input_type'   => $includes[$slug . '_input_type'],
                'Model' => $Model

            ]);

            $rendered_vars[] = $slug;
            $html            = $View->output();
        }

        return (object) compact('html', 'rendered_vars');
    }
}
