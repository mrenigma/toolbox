<?php
namespace Soapbox\Toolbox\Filters\FilterTypes;

use Soapbox\Toolbox\Interfaces\FilterTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;
use Soapbox\Toolbox\Interfaces\ViewInterface;

class FilterTag extends FilterType implements FilterTypesInterface
{

    /**
     * Build the filter View based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface  $View             The view to display the data
     * @param array                                      $includes         Array of settings for the filters
     * @param array                                      $rendered_vars    Array of variables which have been rendered already
     * @param array                                      $passed_variables Array of variable values
     * @param string                                     $slug             The alias of the filter
     * @param string                                     $field_type       The type of field to check against
     *
     * @return object Returns the resulting View output and the rendered variables sub-array
     *                This must return an object with has a minimum of these two properties: html, rendered_vars
     */
    public static function build(ModelInterface $Model, ViewInterface $View, Array $includes, Array $rendered_vars, Array $passed_variables, $slug, $field_type = '')
    {

        $args = [
            'taxonomy'   => 'post_tag',
            'orderby'    => 'menu_order',
            'hide_empty' => 0
        ];

        $tags[]    = $Model->getTags($args);
        $tag_names = [];
        $exclude   = [];
        $html      = '';

        foreach ($tags[0] as $tag) {
            // Check if tag has posts
            if ($tag->count > 0) {
                $tag_names[] = $tag->slug;
            }
        }

        // Overwrite EXCLUDE array using the includes array
        if (isset($includes['tags']) && $includes['tags_include_or_exclude'] === 'include') {
            foreach ($tag_names as $tag_name) {
                if (!in_array($tag_name, $includes['tags'])) {
                    $exclude[] = $tag_name;
                }
            }
        }

        // Write EXCLUDE array using the includes array
        if (isset($includes['tags']) && $includes['tags_include_or_exclude'] === 'exclude') {
            $exclude = $includes['tags'];
        }

        // If include_or_exclude not set, $exclude is empty
        if (!isset($includes['tags_include_or_exclude'])) {
            $exclude = [];
        }

        // If $tags not empty
        // If one or more tags
        // Does not show filter if there is only one filter option - as it wouldn't do anything
        if (!empty($tags) && (count($tags) > 1 || count($tags[0]) > 1)) {
            if (!isset($includes['tags_input_type'])) {
                $includes['tags_input_type'] = '';
            }

            foreach ($tags as $k => $tag_group) {
                $View->setVars([
                    'exclude'      => $exclude,
                    'filters'      => $tag_group,
                    'filter_class' => 'tags',
                    'get_vars'     => $passed_variables,
                    'includes'     => $includes,
                    'select_slug'  => 'tags',
                    'title'        => self::getFilterLabel($includes, 'tags', 'Tags'),
                    'input_type'   => $includes['tags_input_type'],
                    'Model' => $Model
                ]);

                $rendered_vars[] = 'tags';
                $html            = $View->output();
            }
        }

        return (object) compact('html', 'rendered_vars');
    }
}
