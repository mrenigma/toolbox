<?php
namespace Soapbox\Toolbox\Filters\FilterTypes;

use Soapbox\Toolbox\Interfaces\FilterTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;
use Soapbox\Toolbox\Interfaces\ViewInterface;

class FilterCustomTaxonomy extends FilterType implements FilterTypesInterface
{

    /**
     * Build the filter View based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface  $View             The view to display the data
     * @param array                                      $includes         Array of settings for the filters
     * @param array                                      $rendered_vars    Array of variables which have been rendered already
     * @param array                                      $passed_variables Array of variable values
     * @param string                                     $slug             The alias of the filter
     * @param string                                     $field_type       The type of field to check against
     *
     * @return object Returns the resulting View output and the rendered variables sub-array
     *                This must return an object with has a minimum of these two properties: html, rendered_vars
     */
    public static function build(ModelInterface $Model, ViewInterface $View, Array $includes, Array $rendered_vars, Array $passed_variables, $slug, $field_type = '')
    {

        $taxonomies = [
            $slug
        ];

        $taxonomy = $Model->getTaxonomy($slug);
        $html     = '';

        if (!empty($taxonomy)) {
            $post_types = $includes['types'];

            if (!empty($post_types)) {
                $imploded_post_types = implode("','", $post_types);
                $imploded_taxonomies = implode("','", $taxonomies);

                $post_status = $Model->getUserPostStatus();

                $terms = $Model->getCustomTerms($post_status, $imploded_post_types, $imploded_taxonomies);
            } else {
                $args = [
                    'orderby'           => 'name',
                    'order'             => 'ASC',
                    'hide_empty'        => true,
                    'exclude'           => [],
                    'exclude_tree'      => [],
                    'include'           => [],
                    'number'            => '',
                    'fields'            => 'all',
                    'slug'              => '',
                    'parent'            => 0,
                    'hierarchical'      => true,
                    'child_of'          => 0,
                    'get'               => '',
                    'name__like'        => '',
                    'description__like' => '',
                    'pad_counts'        => false,
                    'offset'            => '',
                    'search'            => '',
                    'cache_domain'      => 'core'
                ];

                $terms = $Model->getTerms($taxonomies, $args);
            }

            $include_exclude_key = $slug . '_include_or_exclude';
            $includes_key        = $slug . '_terms';

            // Overwrite EXCLUDE array using the includes array
            if (isset($includes[$includes_key]) && $includes[$include_exclude_key] === 'include') {
                foreach ($terms as $term) {
                    if (!in_array($term->slug, $includes[$includes_key])) {
                        $exclude[] = $term->slug;
                    }
                }
            }

            // Write EXCLUDE array using the includes array
            if (isset($includes[$includes_key]) && $includes[$include_exclude_key] === 'exclude') {
                $exclude = $includes[$includes_key];
            }

            if (empty($exclude)) {
                $exclude = [];
            }

            usort($terms, function ($a, $b) {

                return strcmp($a->name, $b->name);
            });

            if (isset($includes[$slug . '_select_all']) && $includes[$slug . '_select_all']) {
                $select_all = true;
            } else {
                $select_all = false;
            }

            if (!isset($includes[$slug . '_input_type'])) {
                $includes[$slug . '_input_type'] = '';
            }

            $View->setVars([
                'exclude'       => $exclude,
                'filters'       => $terms,
                'filter_class'  => $slug,
                'get_vars'      => $passed_variables,
                'includes'      => $includes,
                'select_prefix' => 'tax',
                'select_slug'   => $slug,
                'title'         => self::getFilterLabel($includes, $slug, $taxonomy->label),
                'select_all'    => $select_all,
                'input_type'    => $includes[$slug . '_input_type'],
                'Model' => $Model
            ]);

            $rendered_vars[] = $slug;
            $html            = $View->output();
        }

        return (object) compact('html', 'rendered_vars');
    }
}
