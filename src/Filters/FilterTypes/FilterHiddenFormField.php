<?php
namespace Soapbox\Toolbox\Filters\FilterTypes;

use Soapbox\Toolbox\Interfaces\FilterTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;
use Soapbox\Toolbox\Interfaces\ViewInterface;

class FilterHiddenFormField extends FilterType implements FilterTypesInterface
{

    /**
     * Build the filter View based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface  $View             The view to display the data
     * @param array                                      $includes         Array of settings for the filters
     * @param array                                      $rendered_vars    Array of variables which have been rendered already
     * @param array                                      $passed_variables Array of variable values
     * @param string                                     $slug             The alias of the filter
     * @param string                                     $field_type       The type of field to check against
     *
     * @return object Returns the resulting View output and the rendered variables sub-array
     *                This must return an object with has a minimum of these two properties: html, rendered_vars
     */
    public static function build(ModelInterface $Model, ViewInterface $View, Array $includes, Array $rendered_vars, Array $passed_variables, $slug, $field_type = '')
    {

        $html = '';

        // Add custom children field values rendered_vars
        foreach ($includes as $inc_key => $inc_val) {
            if (strpos($inc_key, '_children_filter_slug')) {
                $rendered_vars[] = $inc_val;
            }
        }

        foreach ($passed_variables as $k => $pass_v) {
            if (!in_array($k, $rendered_vars)) {
                if ($View->hasVars() || $View->hasOutput()) {
                    $View->reset();
                }

                if (is_array($pass_v)) {
                    foreach ($pass_v as $j => $v) {
                        if ($View->hasVars() || $View->hasOutput()) {
                            $View->reset();
                        }

                        $View->setVars([
                            'name'  => 'hidden-' . $k . '[]',
                            'value' => $v,
                            'Model' => $Model
                        ]);

                        $html .= $View->output();
                    }
                } else {
                    $View->setVars([
                        'name'  => 'hidden-' . $k,
                        'value' => $pass_v,
                        'Model' => $Model
                    ]);

                    $html .= $View->output();
                }
            }
        }

        return (object) compact('html', 'rendered_vars');
    }
}
