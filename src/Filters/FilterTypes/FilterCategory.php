<?php
namespace Soapbox\Toolbox\Filters\FilterTypes;

use Soapbox\Toolbox\Interfaces\FilterTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;
use Soapbox\Toolbox\Interfaces\ViewInterface;

class FilterCategory extends FilterType implements FilterTypesInterface
{

    /**
     * Build the filter View based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface  $View             The view to display the data
     * @param array                                      $includes         Array of settings for the filters
     * @param array                                      $rendered_vars    Array of variables which have been rendered already
     * @param array                                      $passed_variables Array of variable values
     * @param string                                     $slug             The alias of the filter
     * @param string                                     $field_type       The type of field to check against
     *
     * @return object Returns the resulting View output and the rendered variables sub-array
     *                This must return an object with has a minimum of these two properties: html, rendered_vars
     */
    public static function build(ModelInterface $Model, ViewInterface $View, Array $includes, Array $rendered_vars, Array $passed_variables, $slug, $field_type = '')
    {

        // Get all Categories
        $args         = [
            'taxonomy'   => 'category',
            'orderby'    => 'menu_order',
            'hide_empty' => 1
        ];
        $categories[] = $Model->getCategories($args);
        $html         = '';

        // Overwrite EXCLUDE array using the includes array
        if (isset($includes['categories']) && $includes['categories_include_or_exclude'] === 'include') {
            foreach ($categories[0] as $cat) {
                if (!in_array($cat->slug, $includes['categories'])) {
                    $exclude[] = $cat->slug;
                }
            }
        }

        // Write EXCLUDE array using the includes array
        if (isset($includes['categories']) && $includes['categories_include_or_exclude'] === 'exclude') {
            $exclude = $includes['categories'];
        }

        if (empty($exclude)) {
            $exclude = [];
        }

        // If categories exists and has posts in it
        if (!empty($categories) && (count($categories) > 0 || count($categories[0]) > 0)) {
            if (!isset($includes['categories_input_type'])) {
                $includes['categories_input_type'] = '';
            }

            foreach ($categories as $i => $cat_group) {
                $View->setVars([
                    'exclude'      => $exclude,
                    'filters'      => $cat_group,
                    'filter_class' => 'category',
                    'get_vars'     => $passed_variables,
                    'includes'     => $includes,
                    'select_slug'  => 'categories',
                    'title'        => self::getFilterLabel($includes, 'categories', 'Categories'),
                    'input_type'   => $includes['categories_input_type'],
                    'Model'        => $Model
                ]);

                $rendered_vars[] = 'categories';
                $html            = $View->output();
            }
        }

        return (object) compact('html', 'rendered_vars');
    }
}