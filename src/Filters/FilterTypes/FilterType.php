<?php
namespace Soapbox\Toolbox\Filters\FilterTypes;

use Soapbox\Toolbox\Interfaces\ModelInterface;

class FilterType
{

    /**
     * Get the query method name from the slug
     *
     * @param string $slug The name of the method
     *
     * @return string Return formatted query method name
     */
    public static function getQueryMethodName($slug)
    {

        return 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', str_replace('-', ' ', $slug)))) . 'Query';
    }

    /**
     * Determine the filter label
     *
     * @param array  $includes Array of settings for the filters
     * @param string $slug     String to search for
     * @param string $default  The default text to show
     *
     * @return string
     */
    public static function getFilterLabel(Array $includes, $slug = '', $default = '')
    {

        if (!empty($slug) && isset($includes[$slug . '_label'])) {
            return $includes[$slug . '_label'];
        }

        return $default;
    }

    /**
     * Get the post types from the Model and return after processing
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model The model to get the data from
     *
     * @return array Array of processed post types
     */
    public static function getPostTypes(ModelInterface $Model)
    {

        // Args for get_post_types()
        $args = [
            'public' => true,
        ];

        $post_types = $Model->getPostTypes($args, 'objects');

        if (!empty($post_types)) {
            foreach ($post_types as $post_type => $post_Type_object) {
                $post_args = [
                    'post_type'   => $post_type,
                    'post_status' => 'publish',
                    'post_parent' => 0
                ];

                if ($Model->isLoggedIn()) {
                    $post_args['post_status'] = [
                        'publish',
                        'private'
                    ];
                }

                $published_posts = $Model->getPosts($post_args);

                if (empty($published_posts)) {
                    unset($post_types[$post_type]);
                }
            }
        }

        return $post_types;
    }

    /**
     * Get the types to exclude array
     *
     * @param array $post_types List of post types to check
     * @param array $includes   Array of settings for the filters
     *
     * @return array Array of types to exclude
     */
    public static function getTypesExclude(Array $post_types, Array $includes)
    {

        // Normalise the post types for the template
        foreach ($post_types as $post_name) {
            if (empty($post_name->slug)) {
                $post_name->slug = $post_name->name;
            }

            $post_name->name = $post_name->label;
        }

        // Overwrite EXCLUDE array using the include array
        if (isset($includes['types_include_or_exclude']) && $includes['types_include_or_exclude'] === 'include') {
            foreach ($post_types as $post_name) {
                if (!in_array($post_name->slug, $includes['types'])) {
                    $type_exclude[] = $post_name->slug;
                }
            }
        }

        if (isset($includes['types_include_or_exclude']) && $includes['types_include_or_exclude'] === 'exclude') {
            $type_exclude = $includes['types'];
        }

        if (empty($type_exclude)) {
            $type_exclude = [];
        }

        return $type_exclude;
    }

    /**
     * Get the filter option label
     *
     * @param array  $includes Array of settings for the filters
     * @param string $prefix   The slug to search for
     * @param string $default  The default value if the $prefix is not found
     *
     * @return string Returns the option label
     */
    public static function getFilterOptionLabel(Array $includes, $prefix = '', $default = '')
    {

        if (!empty($prefix) && isset($includes[$prefix . '_option_label'])) {
            return $includes[$prefix . '_option_label'];
        }

        return $default;
    }

    /**
     * Get the stored string from $includes
     *
     * @param array  $includes Array of settings for the filters
     * @param string $key      The slug to search for
     * @param string $default  The default value if the $key is not found
     *
     * @return string Returns the stored string
     */
    public static function getIniString(Array $includes, $key = '', $default = '')
    {

        if (!empty($key) && isset($includes[$key])) {
            return $includes[$key];
        }

        return $default;
    }

    /**
     * Get the children filter slug
     *
     * @param array  $includes Array of settings for the filters
     * @param string $prefix   The slug to search for
     * @param string $default  The default value if the $prefix is not found
     *
     * @return string Returns the children filter slug
     */
    public static function getChildrenFilterSlug(Array $includes, $prefix = '', $default = '')
    {

        if (!empty($prefix) && isset($includes[$prefix . '_children_filter_slug'])) {
            return $includes[$prefix . '_children_filter_slug'];
        }

        return $default;
    }

    /**
     * Get the filters custom children
     *
     * @param array  $includes Array of settings for the filters
     * @param string $prefix   The slug to search for
     *
     * @return array Returns the array of custom children
     */
    public static function getFilterCustomChildren(Array $includes, $prefix = '')
    {

        $return_array = [];

        if (!empty($prefix) && isset($includes[$prefix . '_add_children_slugs']) && isset($includes[$prefix . '_add_children_names'])) {
            $return_array = array_combine($includes[$prefix . '_add_children_slugs'], $includes[$prefix . '_add_children_names']);
        }

        return $return_array;
    }
}
