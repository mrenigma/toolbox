<?php
namespace Soapbox\Toolbox\Filters\FilterTypes;

use Soapbox\Toolbox\Interfaces\FilterTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;
use Soapbox\Toolbox\Interfaces\ViewInterface;

class FilterYear extends FilterType implements FilterTypesInterface
{

    /**
     * Build the filter View based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface  $View             The view to display the data
     * @param array                                      $includes         Array of settings for the filters
     * @param array                                      $rendered_vars    Array of variables which have been rendered already
     * @param array                                      $passed_variables Array of variable values
     * @param string                                     $slug             The alias of the filter
     * @param string                                     $field_type       The type of field to check against
     *
     * @return object Returns the resulting View output and the rendered variables sub-array
     *                This must return an object with has a minimum of these two properties: html, rendered_vars
     */
    public static function build(ModelInterface $Model, ViewInterface $View, Array $includes, Array $rendered_vars, Array $passed_variables, $slug, $field_type = '')
    {

        $post_types   = self::getPostTypes($Model);
        $type_exclude = self::getTypesExclude($post_types, $includes);
        $limit        = false;

        if (!empty($includes['years_archive']) && $includes['years_archive'] === '1' && !empty($includes['years_limit'])) {
            $limit = $includes['years_limit'];
        }

        if (isset($includes['years_field']) && !empty($includes['years_field'])) {
            $years_field = $includes['years_field'];
        } else {
            $years_field = 'post_date';
        }

        $years = $Model->getYears($type_exclude, true, $limit, $years_field);

        // Normalise the years for the template
        foreach ($years as $year) {
            $year->slug = $year->post_year;
            $year->name = $year->post_year;
        }

        // Overwrite EXCLUDE array using the includes array
        if (isset($includes['years']) && $includes['years_include_or_exclude'] === 'include') {

            foreach ($years as $year) {
                if (!in_array($year->slug, $includes['years'])) {
                    $exclude[] = $year->slug;
                }
            }
        }

        // Write EXCLUDE array using the includes array
        if (isset($includes['years']) && $includes['years_include_or_exclude'] === 'exclude') {
            $exclude = $includes['years'];
        }

        if (empty($exclude)) {
            $exclude = [];
        }

        if (!isset($includes['years_input_type'])) {
            $includes['years_input_type'] = '';
        }

        $View->setVars([
            'exclude'      => $exclude,
            'filters'      => $years,
            'filter_class' => 'year',
            'get_vars'     => $passed_variables,
            'includes'     => $includes,
            'select_slug'  => 'year',
            'title'        => self::getFilterLabel($includes, 'years', 'Year'),
            'input_type'   => $includes['years_input_type'],
            'Model'        => $Model
        ]);

        $rendered_vars[] = 'year';
        $html            = $View->output();

        return (object) compact('html', 'rendered_vars');
    }
}
