<?php
namespace Soapbox\Toolbox\Filters\FilterTypes;

use Soapbox\Toolbox\Interfaces\FilterTypesInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;
use Soapbox\Toolbox\Interfaces\ViewInterface;

class FilterPostType extends FilterType implements FilterTypesInterface
{

    /**
     * Build the filter View based on the include values and settings
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model            The model to retrieve data
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface  $View             The view to display the data
     * @param array                                      $includes         Array of settings for the filters
     * @param array                                      $rendered_vars    Array of variables which have been rendered already
     * @param array                                      $passed_variables Array of variable values
     * @param string                                     $slug             The alias of the filter
     * @param string                                     $field_type       The type of field to check against
     *
     * @return object Returns the resulting View output and the rendered variables sub-array
     *                This must return an object with has a minimum of these two properties: html, rendered_vars
     */
    public static function build(ModelInterface $Model, ViewInterface $View, Array $includes, Array $rendered_vars, Array $passed_variables, $slug, $field_type = '')
    {

        $post_types = self::getPostTypes($Model);

        // Sort by Post Type -> labels -> name
        usort($post_types, function ($a, $b) {

            return strcmp($a->labels->name, $b->labels->name);
        });

        $type_exclude = self::getTypesExclude($post_types, $includes);

        if (!isset($includes['post_type_input_type'])) {
            $includes['post_type_input_type'] = '';
        }

        $View->setVars([
            'exclude'      => $type_exclude,
            'filters'      => $post_types,
            'filter_class' => 'post-type',
            'get_vars'     => $passed_variables,
            'includes'     => $includes,
            'select_slug'  => 'post_type',
            'title'        => self::getFilterLabel($includes, 'types', 'Post Type'),
            'input_type'   => $includes['post_type_input_type'],
            'Model' => $Model
        ]);

        $rendered_vars[] = 'post_type';
        $html            = $View->output();

        return (object) compact('html', 'rendered_vars');
    }
}
