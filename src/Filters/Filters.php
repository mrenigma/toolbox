<?php

namespace Soapbox\Toolbox\Filters;

use Soapbox\Toolbox\Interfaces\FiltersInterface;
use Soapbox\Toolbox\Interfaces\ModelInterface;
use Soapbox\Toolbox\Interfaces\PathInterface;
use Soapbox\Toolbox\Interfaces\ViewInterface;

/**
 * Class Filters
 * A class to convert the configuration into viewable Filters and Sorters.
 * Uses default views unless otherwise specified.
 *
 * @package Soapbox\Toolbox\Filters
 * @see     Soapbox\Toolbox\Interfaces\FiltersInterface
 */
class Filters implements FiltersInterface
{

    /**
     * @var \Soapbox\Toolbox\Interfaces\ModelInterface $Model The model to get data from
     */
    protected $Model;

    /**
     * @var \Soapbox\Toolbox\Interfaces\PathInterface $Path The class to verify the set paths
     */
    protected $Path;

    /**
     * @var \Soapbox\Toolbox\Interfaces\ViewInterface $View The view to display the data
     */
    protected $View;

    /**
     * @var array $passed_variables Store years SQL Results if already queried
     */
    public $passed_variables;

    /**
     * @var array $function_views Array of views to use for each function
     */
    public $function_views = [
        'buildFilters'          => 'filter-form',
        'buildSorters'          => 'sorters',
        'buildActiveFilters'    => 'active-filters',
        'FilterHiddenFormField' => 'hidden-fields',
        'FilterButton'          => 'buttons',
        'FilterCategory'        => 'filters',
        'FilterPostType'        => 'filters',
        'FilterTag'             => 'filters',
        'FilterYear'            => 'filters',
        'FilterCustomField'     => 'filters',
        'FilterCustomTaxonomy'  => 'filters',
        'FilterPostStatus'      => 'post-status-filter',
    ];

    /**
     * @var array $extra_filter_types Array of class name => path pairs to check over for potential filter types
     */
    public $extra_filter_types = [];

    /**
     * @var string $template_path The path to the template
     */
    public $template_path = '';

    /**
     * @var array $template_paths Array of paths to where the templates are located
     */
    public $template_paths = [
        'assets/includes/vendor/sb-dev-team/toolbox/src/Filters/FilterTemplates/',
        'assets/includes/Toolbox/src/Filters/FilterTemplates/'
    ];

    /**
     * @var string $template_ext The template file extension
     */
    public $template_ext = '.php';

    /**
     * @var string $html Store the output
     */
    public $html = '';

    /**
     * @var array $rendered_vars Array of the variables that have been rendered already
     */
    public $rendered_vars = [];

    /**
     * Set the passed common arguments class
     * Set this classes passed variables
     *
     * @param \Soapbox\Toolbox\Interfaces\ModelInterface $Model              The model to get data from
     * @param \Soapbox\Toolbox\Interfaces\PathInterface  $Path               The class to verify the set paths
     * @param \Soapbox\Toolbox\Interfaces\ViewInterface  $View               The view to display the data
     * @param array                                      $passed_vars        Array of variable values built in Soapbox\Toolbox\Args\ArgsCommon
     * @param array                                      $function_views     Array of views to use for each function
     * @param array                                      $extra_filter_types Array of class name => path pairs to dynamically add new filter types
     * @param array|string                               $template_path      Array or string of path(s) to where the templates are located
     * @param string                                     $template_ext       The template file extension
     */
    public function __construct(
        ModelInterface $Model,
        PathInterface $Path,
        ViewInterface $View,
        Array $passed_vars = [],
        Array $function_views = [],
        Array $extra_filter_types = [],
        $template_path = [
            'assets/includes/vendor/sb-dev-team/toolbox/src/Filters/FilterTemplates',
            'assets/includes/Toolbox/src/Filters/FilterTemplates/'
        ],
        $template_ext = '.php'
    ) {

        $this->Model = $Model;
        $this->Path  = $Path;
        $this->View  = $View;

        if (!empty($function_views)) {
            $this->function_views = array_merge($this->function_views, $function_views);
        }

        if (is_array($template_path)) {
            $this->template_paths = array_unique(array_merge($template_path, $this->template_paths));
        } else {
            $this->template_path = $template_path;
        }

        $this->template_ext = $template_ext;

        // Retrieve Passed Vars array
        $this->passed_variables = $passed_vars;

        // Store the new filter types
        $this->extra_filter_types = $this->validateExtraFilterTypes($extra_filter_types);
    }

    /**
     * Add a new path to the beginning of the $template_paths array
     *
     * @param \Soapbox\Toolbox\Interfaces\PathInterface $Path The location of the files
     *
     * @return \Soapbox\Toolbox\Interfaces\FiltersInterface Returns itself
     */
    public function addPath($Path)
    {

        if ($Path->checkPath()) {
            $path = $Path->getRealPath();

            if (!in_array($path, $this->template_paths)) {
                array_unshift($this->template_paths, $path);
            }
        }

        return $this;
    }

    /**
     * Get the class name which this class understands
     *
     * @param string $slug The base class name
     *
     * @return string The class name that works with this class
     */
    private function getClassName($slug)
    {

        if ($slug === 'categories') {
            $slug = 'category';
        } else if ($slug === 'tag') {
            $slug = 'post_tag';
        }

        return 'Filter' . str_replace(' ', '', ucwords(str_replace('_', ' ', str_replace('-', ' ', $slug))));
    }

    /**
     * Check the extra filter types to make sure they exist, if not remove them from the array
     *
     * @param array $extra_filter_types
     *
     * @return array Returns validated extra arg types
     */
    private function validateExtraFilterTypes(Array $extra_filter_types)
    {

        foreach ($extra_filter_types as $class => $path) {
            /**
             * @var \Soapbox\Toolbox\Interfaces\PathInterface $path_check
             */
            $path_check = $this->Path->setPath($path);

            if ($path_check->checkPath()) {
                $extra_filter_types[$class] = $path_check->getRealPath();
            } else {
                unset($extra_filter_types[$class]);
            }
        }

        return $extra_filter_types;
    }

    /**
     * Check to see if the filter type exists in the $extra_filter_types array
     *
     * @param string $class_name The class name ot search for
     *
     * @return bool Returns true/false if the class exists/does not exist
     */
    private function extraFilterTypeExists($class_name)
    {

        if (!empty($this->extra_filter_types[$class_name])) {
            return $this->extra_filter_types[$class_name];
        }

        return false;
    }

    /**
     * Check to see if the filter type exists in the autoloaded classes
     *
     * @param string $class_name The class name to search for
     *
     * @return bool|string Returns the class FQN or false if no class exists
     */
    protected function filterTypeExists($class_name)
    {

        $name   = __NAMESPACE__ . '\FilterTypes\\' . $class_name;
        $exists = class_exists($name);

        if ($exists) {
            return $name;
        }

        return false;
    }

    /**
     * Get the class to process the filter
     *
     * @param string $slug The base slug of the class
     *
     * @return bool|string Returns the FQN for the class
     */
    private function getFilterClass($slug)
    {

        $name       = $this->getClassName($slug);
        $class_name = $this->extraFilterTypeExists($name);

        if (!empty($class_name)) {
            /** @noinspection PhpIncludeInspection */
            include_once $class_name;

            $class_name = $name;
        } else {
            $class_name = $this->filterTypeExists($name);
        }

        return $class_name;
    }

    /**
     * Get the filter type
     *
     * @param string  $class         The FQN of the class
     * @param array   $includes      Array of settings for the filters
     * @param string  $function_name The function name to get the view for
     * @param string  $slug          The slug of the arg type to process
     * @param  string $field_type    The type of field to check against
     * @param string  $method        The method to use within the FQN class
     *
     * @return string Returns the new filter types output
     */
    protected function getFilterType($class, $function_name, Array $includes, $slug, $field_type = '', $method = 'build')
    {

        if (!empty($class)) {
            $View = new $this->View($this->Path, $this->template_path . $this->function_views[$function_name], $this->template_paths, $this->template_ext);

            /** @noinspection PhpUndefinedMethodInspection */
            $response = $class::{$method}($this->Model, $View, $includes, $this->rendered_vars, $this->passed_variables, $slug, $field_type);

            $this->rendered_vars = array_merge($this->rendered_vars, $response->rendered_vars);

            return $response->html;
        }

        return '';
    }

    /**
     *  buildFilters method to automatically generate a filters form.
     *
     * @param array  $filter_items   Array of filters to be shown.
     *                               Accepted values: 'category', 'post_type', 'tags', 'year', 'author'
     * @param array  $includes       Settings for the filters
     * @param string $extra_elements Any HTML elements to add to the end of the form but before the form end
     * @param string $pre_elements   Any HTML elements to add to the beginning of the form but after the form start
     *
     * @return string $html  HTML of the filter form that is outputted to the page
     */
    public function buildFilters(Array $filter_items, Array  $includes = [], $extra_elements = '', $pre_elements = '')
    {

        /**
         * @var \Soapbox\Toolbox\Interfaces\ViewInterface $View
         */
        $View = new $this->View($this->Path, $this->template_path . $this->function_views[__FUNCTION__], $this->template_paths, $this->template_ext);
        $View->setVars(['get_vars' => $this->passed_variables]);

        $form_elements = '';

        if (!empty($pre_elements)) {
            $form_elements .= $pre_elements;
        }

        foreach ($filter_items as $l => $filter) {
            if ($filter === 'tags') {
                $name = 'tag';
            } else {
                $name = $filter;
            }

            $class_name = $this->getClassName($name);
            $class      = $this->getFilterClass($name);

            // If it's a defined ArgType
            if (!empty($class)) {
                $form_elements .= $this->getFilterType($class, $class_name, $includes, $filter, $filter);
            } else {
                // Try to do it as a Custom Field
                if (isset($includes[$filter . '_type']) && $includes[$filter . '_type'] === 'customfield') {
                    $form_elements .= $this->getFilterType($this->getFilterClass('custom_field'), $this->getClassName('custom_field'), $includes, $filter, $includes[$filter . '_field_type']);
                } else if (isset($includes[$filter . '_include_or_exclude'])) {
                    $form_elements .= $this->getFilterType($this->getFilterClass('custom_taxonomy'), $this->getClassName('custom_taxonomy'), $includes, $filter, $filter);
                }
            }
        }

        if (!empty($extra_elements)) {
            $form_elements .= $extra_elements;
        }

        // Add hidden sort-by form fields.
        $form_elements .= $this->getFilterType($this->getFilterClass('hidden_form_field'), $this->getClassName('hidden_form_field'), $includes, 'hidden_form_field', 'hidden_field');

        // Add buttons if needed
        $form_elements .= $this->getFilterType($this->getFilterClass('button'), $this->getClassName('button'), $includes, 'button', 'button');

        $View->setVar('form_elements', $form_elements);
        $View->setVar('includes', $includes);

        $this->html .= $View->output();

        return $this->html;
    }

    /**
     * buildSorters method to automatically generate a sorting form.
     *
     * @return string HTML of the sorters form that is outputted to the page
     */
    public function buildSorters()
    {

        /**
         * @var \Soapbox\Toolbox\Interfaces\ViewInterface $View
         */
        $View = new $this->View($this->Path, $this->template_path . $this->function_views[__FUNCTION__], $this->template_paths, $this->template_ext);
        $View->setVars([
            'get_vars' => $this->passed_variables,
            'exclude'  => []
        ]);

        $html = $View->output();

        return $html;
    }

    /**
     * Clear the stored $html and the $rendered_vars array
     */
    public function clearHtml()
    {

        $this->html          = '';
        $this->rendered_vars = [];
    }

}