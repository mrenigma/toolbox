<?php
namespace Soapbox\Toolbox\Filters;

/**
 * Class EventFilters
 *
 * @package Soapbox\Toolbox\Filters
 * @see     Soapbox\Toolbox\Filters\Filters
 */
class EventFilters extends Filters
{

    /**
     * Automatically generate the events sorting form.
     *
     * @return string
     */
    public function buildSorters()
    {

        /**
         * @var \Soapbox\Toolbox\Interfaces\ViewInterface $View
         */
        $View = new $this->View($this->Path, $this->template_path . 'sorters-event', $this->template_paths, $this->template_ext);

        $View->setVars([
            'get_vars' => $this->passed_variables,
            'exclude'  => []
        ]);

        $html = $View->output();

        return $html;
    }
}
