<?php
namespace Soapbox\Toolbox\Models;

use Soapbox\Toolbox\Interfaces\ModelInterface;

/**
 * Class WordPress
 * A class to store the WordPress specific functionality and DB queries. Acts like a pseudo-model
 *
 * @package Soapbox\Toolbox\Models
 */
class WordPress implements ModelInterface
{

    /**
     * @var array $years Store years SQL Results if already queried
     * */
    private $years = [];

    /** @noinspection PhpUndefinedClassInspection */
    /**
     * @var \wpdb $db Store of the DB connection
     */
    private $db;

    /**
     * Set the WordPress wpdb global to $this->db
     */
    public function __construct()
    {

        global $wpdb;

        $this->db = $wpdb;
    }

    /**
     * Get the results of the query and return
     *
     * @param string $query The query string to process
     * @param mixed  $type  The type of output that should be returned
     *
     * @return mixed Return the results of the query
     */
    public function getResults($query, $type = null)
    {

        if (defined('OBJECT') && empty($type)) {
            $type = OBJECT;
        }

        if (method_exists($this->db, 'get_results')) {
            return $this->db->get_results($query, $type);
        } else {
            return false;
        }
    }

    /**
     * Check to see if the user is logged in or not
     *
     * @return bool True/False the user is logged in
     */
    public function isLoggedIn()
    {

        if (function_exists('is_user_logged_in') && function_exists('get_current_user_id')) {
            return (is_user_logged_in() && get_current_user_id());
        } else {
            return false;
        }
    }

    /**
     * Check to see if the taxonomy is hierarchical or not based on the slug
     *
     * @param string $slug The alias of the taxonomy
     *
     * @return bool Returns whether the taxonomy is/is not hierarchical
     */
    public function isTaxonomyHierarchical($slug)
    {

        if (function_exists('is_taxonomy_hierarchical')) {
            return is_taxonomy_hierarchical($slug);
        } else {
            return false;
        }
    }

    /**
     * Get the users post statuses based on whether they are logged in or not
     *
     * @return string The post statuses for the user
     */
    public function getUserPostStatus()
    {

        if ($this->isLoggedIn()) {
            $post_status = "'publish','private'";
        } else {
            $post_status = "'publish'";
        }

        return $post_status;
    }

    /**
     * Find all post types based on the $args, returned array values are based on the $output_type
     *
     * @param array  $args
     * @param string $output_type
     *
     * @return array Array of post types
     */
    public function getPostTypes(Array $args = [], $output_type = 'names')
    {

        if (function_exists('get_post_types')) {
            return get_post_types($args, $output_type);
        } else {
            return [];
        }
    }

    /**
     * Get all the posts
     *
     * @param array $args
     *
     * @return array Array of posts
     */
    public function getPosts(Array $args = [])
    {

        if (function_exists('get_posts')) {
            return get_posts($args);
        } else {
            return [];
        }
    }

    /**
     * Get a list of categories based on the $args
     *
     * @param array $args
     *
     * @return array Array of categories
     */
    public function getCategories(Array $args = [])
    {

        if (function_exists('get_categories')) {
            return get_categories($args);
        } else {
            return [];
        }
    }

    /**
     * Get a list of tags based on the $args
     *
     * @param array $args
     *
     * @return array Array of tags
     */
    public function getTags(Array $args = [])
    {

        if (function_exists('get_tags')) {
            return get_tags($args);
        } else {
            return [];
        }
    }

    /**
     * Get the taxonomy by its slug
     *
     * @param string $slug Alias of taxonomy
     *
     * @return bool|object Returns the object if found otherwise it will return false
     */
    public function getTaxonomy($slug)
    {

        if (function_exists('get_taxonomy')) {
            return get_taxonomy($slug);
        } else {
            return false;
        }
    }

    /**
     * Get a list of terms for taxonomy
     *
     * @param array $taxonomies Array of taxonomies to get terms for
     * @param array $args       Array of arguments constraining the resulting array
     *
     * @return array Array of terms
     */
    public function getTerms(Array $taxonomies, Array $args = [])
    {

        if (function_exists('get_terms')) {
            return get_terms($taxonomies, $args);
        } else {
            return [];
        }
    }

    /**
     * Get a term by the $field, $value and $taxonomy
     *
     * @param string $field    The name of the field to find
     * @param string $value    The value of the field which the $field must equal
     * @param string $taxonomy The taxonomy the $field must reside in
     *
     * @return mixed Returns term or false if $taxonomy does not exist or the term was not found.
     */
    public function getTermBy($field = 'slug', $value = '', $taxonomy = 'post_tag')
    {

        if (function_exists('get_term_by')) {
            return get_term_by($field, $value, $taxonomy);
        } else {
            return false;
        }
    }

    /** @noinspection PhpUndefinedClassInspection */
    /**
     * Get the terms child elements
     *
     * @param int|string $term_id       The ID of the term
     * @param string     $taxonomy_slug The alias of the taxonomy to search against
     *
     * @return array|\WP_Error Returns an array of child elements OR an error
     */
    public function getTermChildren($term_id, $taxonomy_slug = '')
    {

        if (function_exists('sb_get_term_children')) {
            $children = sb_get_term_children($term_id);
        } else if (function_exists('get_term_children')) {
            $children = get_term_children($term_id, $taxonomy_slug);
        } else {
            $children = [];
        }

        return $children;
    }

    /**
     * Get option by $name
     *
     * @param string $name    Name of the option
     * @param mixed  $default Default value if no value for the option is found
     *
     * @return mixed The value of the option if found otherwise the default value
     */
    public function getOption($name, $default = false)
    {

        if (function_exists('get_option')) {
            return get_option($name, $default);
        } else {
            return $default;
        }
    }

    /**
     * Get custom terms based on the post status, post types and taxonomies
     *
     * @param string $post_status A comma delimited list of post statuses to check against
     * @param string $post_types  A comma delimited list of post types to check against
     * @param string $taxonomies  A comma delimited list of taxonomies to check against
     *
     * @return array Returns results from DB query
     */
    public function getCustomTerms($post_status, $post_types, $taxonomies)
    {

        /** @noinspection PhpUndefinedFieldInspection */
        $query = <<<SQL
                            SELECT t.* from {$this->db->terms} AS t
        INNER JOIN {$this->db->term_taxonomy} AS tt ON t.term_id = tt.term_id AND tt.parent = 0
        INNER JOIN {$this->db->term_relationships} AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
        INNER JOIN {$this->db->posts} AS p ON p.ID = r.object_id
        WHERE p.post_status IN ({$post_status}) AND p.post_type IN('{$post_types}') AND tt.taxonomy IN('{$taxonomies}')
        GROUP BY t.term_id
SQL;

        return $this->getResults($query);
    }

    /**
     * @param array  $includes      Array of items to include in the query
     * @param array  $custom_fields Array of custom fields
     * @param string $slug          Taxonomy slug
     *
     * @return array Array of custom field ID's
     */
    public function getCustomFieldIds(Array $includes, Array $custom_fields, $slug)
    {

        if (isset($includes[$slug . '_value_type']) && $includes[$slug . '_value_type'] === 'slug') {
            $query_custom_field_ids = '("' . implode('","', $custom_fields) . '")';

            if (isset($includes[$slug . '_post_types'])) {
                if (is_array($includes[$slug . '_post_types'])) {
                    $post_type_query = 'AND
                    post_type IN ("' . implode('","', $includes[$slug . '_post_types']) . '")';
                } else {
                    $post_type_query = 'AND
                    post_type = "' . $includes[$slug . '_post_types'] . '"';
                }
            } else {
                $post_type_query = '';
            }

            $post_status = $this->getUserPostStatus();

            /** @noinspection PhpUndefinedFieldInspection */
            $query = <<<SQL
          SELECT
            ID
          FROM
            {$this->db->posts}
          WHERE
            post_name IN {$query_custom_field_ids}
          AND
            post_status IN ({$post_status})
          {$post_type_query};
SQL;

            $cf_ids = $this->getResults($query);

            foreach ($cf_ids as $k => $cf_id) {
                $cf_ids[$k] = $cf_id->ID;
            }
        } else {
            $cf_ids = $custom_fields;
        }

        return $cf_ids;
    }

    /**
     * return the common custom date is configured in options otherwise post_date
     *
     * @return string
     */
    public function getDateField()
    {

        $dateField = $this->getOption('custom_date_field');

        if (empty($dateField)) {
            $dateField = 'post_date';
        }

        return $dateField;
    }

    /**
     * @param array $order      Array of order parameters
     * @param array $attributes New order attributes to merge into the $order array
     *
     * @return array New $order array
     */
    public function setOrderAttributes(Array $order, Array $attributes = [])
    {

        return array_merge($order, $attributes);
    }

    /**
     * Set the default order attributes
     *
     * @return array Array of default order attributes
     */
    public function getDefaultOrder()
    {

        // gets the common date field configured in the date field options
        $dateField = $this->getDateField();

        if ($dateField === 'post_date') {
            // Set the default sorting value
            $order = [
                'order'   => 'DESC',
                'orderby' => 'date'
            ];
        } else {
            $order = [
                'meta_key'  => $dateField,
                'meta_type' => 'DATE',
                'order'     => 'DESC',
                'orderby'   => 'meta_value'
            ];
        }

        return $order;
    }

    /**
     * Get the correct order attributes based on $sort_by
     *
     * @param array   $order      Array of order attributes current set
     * @param  string $sort_by    The string to sort by
     * @param string  $date_field The date field to use
     *
     * @return array Array of new order attributes
     */
    public function getOrder(Array $order = [], $sort_by, $date_field = 'post_date')
    {

        switch ($sort_by) {
            case 'ndate':
            case  'odate':
                if ($date_field === 'post_date') {
                    $order = $this->setOrderAttributes($order, ['orderby' => 'date']);
                } else {
                    $order = $this->setOrderAttributes($order, [
                        'meta_key'  => $date_field,
                        'meta_type' => 'DATE',
                        'orderby'   => 'meta_value'
                    ]);
                }

                if ($sort_by === 'ndate') {
                    $sort = 'DESC';
                } else {
                    $sort = 'ASC';
                }

                $order = $this->setOrderAttributes($order, ['order' => $sort]);

                break;
            case 'event-ndate':
            case 'event-odate':
                if ($sort_by === 'event-ndate') {
                    $sort = 'DESC';
                } else {
                    $sort = 'ASC';
                }

                $order = $this->setOrderAttributes($order, [
                    'orderby'  => 'meta_value_num',
                    'meta_key' => 'end_date',
                    'order'    => $sort
                ]);

                break;

            case 'title':
            case 'titleaz':
            case 'titleza':
                if ($sort_by !== 'titleza') {
                    $sort = 'ASC';
                } else {
                    $sort = 'DESC';
                }

                $order = $this->setOrderAttributes($order, [
                    'orderby' => 'title',
                    'order'   => $sort
                ]);

                break;

            case 'rate-asc':
            case 'rate-desc':
                if ($sort_by === 'rate-asc') {
                    $sort = 'ASC';
                } else {
                    $sort = 'DESC';
                }

                $order = $this->setOrderAttributes($order, [
                    'orderby'  => 'meta_value_num',
                    'meta_key' => 'rate',
                    'order'    => $sort
                ]);

                break;

            case 'exp-asc':
            case 'exp-desc':
                if ($sort_by === 'exp-asc') {
                    $sort = 'ASC';
                } else {
                    $sort = 'DESC';
                }

                $order = $this->setOrderAttributes($order, [
                    'orderby'  => 'meta_value_num',
                    'meta_key' => 'years_of_experience',
                    'order'    => $sort
                ]);

                break;

            default:
                $order = $this->setOrderAttributes($order, [
                    'orderby' => 'date',
                    'order'   => 'DESC'
                ]);
        }

        return $order;
    }

    /**
     * Retrieve and cache the years of posts.
     *
     * @param               array   $exclude_types Types to be excluded from query
     * @param               boolean $force         Force the creation/update of the cache
     * @param boolean | int         $limit         Limit the number of years shown
     * @param               string  $field         The field name to get years from
     *
     * @return array Array of years
     */
    public function getYears(Array $exclude_types = [], $force = false, $limit = false, $field = 'post_date')
    {

        if ($field === 'post_date') {
            $field = $this->getDateField();
        }

        if (empty($this->years) || $force) {
            // Query the Database to get a list of YEARS
            $query = $this->getYearQuery($field, $exclude_types, $limit);
            $years = $this->getResults($query);

            // Store years as a private variable
            $this->years = $years;
        } else {
            // Pull years from private variable
            $years = $this->years;
        }

        return $years;
    }

    /**
     * Get a list of archive years set by archive and limit field
     *
     * @param     string $field date field
     * @param int        $max   limit to the maximum number of records
     *
     * @return array
     */
    public function getArchiveYears($field = 'default', $max = 5)
    {

        $post_status = $this->getUserPostStatus();

        if ($field === 'default') {
            $sql_query = "SELECT DISTINCT YEAR(post_date) AS post_year ";
            $sql_query .= "FROM wp_posts WHERE post_status IN ({$post_status})";
            $all_years = $this->getResults($sql_query);

            if (count($all_years) > $max) {
                $limit = count($all_years) - $max;
                $years = $this->getResults("SELECT DISTINCT YEAR(post_date) AS post_year FROM wp_posts WHERE post_status IN ({$post_status}) order by post_year DESC LIMIT $max, $limit");
            }
        } else {
            $all_years = $this->getResults("SELECT DISTINCT LEFT(`p2`.`meta_value`,4) AS `post_year` FROM `wp_postmeta` AS `p2` LEFT JOIN `wp_posts` AS `p` ON `p2`.`post_id` = `p`.`ID` WHERE `p2`.`meta_key` =
            '" . $field . "' ORDER BY `post_year` DESC");

            if (count($all_years) > $max) {
                $limit = count($all_years) - $max;
                $years = $this->getResults("SELECT distinct LEFT(p2.meta_value,4) as post_year FROM wp_postmeta as p2 LEFT JOIN wp_posts AS p ON p2.post_id = p.ID WHERE p2.meta_key = '" . $field . "' order by
       post_year DESC LIMIT $max, $limit");
            }
        }

        $return_years = [];

        if (!empty($years)) {
            foreach ($years as $year) {
                $return_years[] = $year->post_year;
            }
        }

        return $return_years;
    }

    /**
     * Set the posts where query
     *
     * @param array  $includes Array of items to include in the posts where
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return ModelInterface Returns the class in use
     */
    public function setPostWhere(Array $includes, $slug)
    {

        if (function_exists('add_filter')) {
            add_filter('posts_where', function ($where) use ($includes, $slug) {

                if (isset($includes[$slug . '_meta_key'])) {
                    $where = str_replace("meta_key = '" . $includes[$slug . '_meta_key'] . "'", "meta_key LIKE '" . $includes[$slug . '_meta_key'] . "'", $where);
                }

                return $where;
            });
        }

        return $this;
    }

    /**
     * Get the query string for the meta key
     *
     * @param array  $includes Array of settings for the filters
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return string Post type query string
     */
    public function getMetaKeyQuery(Array $includes, $slug)
    {

        return 'LIKE "' . $includes[$slug . '_meta_key'] . '"';
    }

    /**
     * Get the query string for post types
     *
     * @param array  $includes Array of settings for the filters
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return string Post type query string
     */
    public function getPostTypesQuery(Array $includes, $slug)
    {

        $post_type_query = '';
        $post_types      = $includes[$slug . '_post_types'];

        if (isset($post_types)) {
            if (is_array($post_types)) {
                $post_type_query = 'AND `p`.`post_type` IN ("' . implode('","', $post_types) . '")';
            } else {
                $post_type_query = 'AND `p`.`post_type` = "' . $post_types . '"';
            }
        }

        return $post_type_query;
    }

    /**
     * Get the query string for filters
     *
     * @param array  $includes Array of settings for the filters
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return string Post type query string
     */
    public function getFiltersQuery(Array $includes, $slug)
    {

        $filter_query = '';

        if (!empty($includes[$slug . '_filter_field']) && !empty($includes[$slug . '_filter_default'])) {
            $vars = filter_input_array(INPUT_GET);

            if (!empty($vars['select-' . $includes[$slug . '_filter_field']])) {
                $val = $vars['select-' . $includes[$slug . '_filter_field']][0];
            } else {
                $val = $includes[$slug . '_filter_default'];
            }

            $filter_query = " AND ID IN (select post_id from wp_postmeta where meta_key = '" . $includes[$slug . '_filter_field'] . "' and meta_value='" . $val . "')";
        }

        return $filter_query;
    }

    /**
     * Get the query string for categories
     *
     * @param array  $includes Array of settings for the filters
     * @param string $slug     Used to retrieve specific items from the $includes array
     *
     * @return string Post type query string
     */
    public function getCategoriesQuery(Array $includes, $slug)
    {

        $category_query = '';

        if (isset($includes[$slug . '_category'])) {
            $category       = $includes[$slug . '_category'];
            $category_query = " AND p.ID IN (select object_id from wp_term_relationships,wp_term_taxonomy where wp_term_relationships.term_taxonomy_id=wp_term_taxonomy.term_taxonomy_id and wp_term_taxonomy.term_id IN (select term_id from wp_terms where slug IN ('" . $category . "') ) ) ";
        }

        return $category_query;
    }

    /**
     * Generate year query based on the date fields
     *
     * @param                string $field         The field name to get years from
     * @param array                 $exclude_types Types to be excluded from query
     * @param  boolean | int        $limit         Limit the number of years shown
     *
     * @return string The MySQL query
     */
    public function getYearQuery($field, Array $exclude_types = [], $limit = false)
    {

        $post_status = $this->getUserPostStatus();

        $post_type_query = '';

        if (!empty($type_exclude)) {
            $post_type_query .= ' AND post_type NOT IN ("' . implode('", "', $type_exclude) . '") ';
        }

        if ($field === 'post_date') { // default post_date query
            $sql_query = "SELECT DISTINCT YEAR($field) AS post_year FROM wp_posts WHERE post_status IN ({$post_status}) ";
            $sql_query .= $post_type_query;
            $sql_query .= ' GROUP BY post_year DESC ';

        } else { // custom date field query
            $sql_query = "SELECT DISTINCT LEFT(meta_value, 4) AS post_year FROM wp_postmeta LEFT JOIN wp_posts ON post_id = ID WHERE meta_key = '{$field}' ";
            $sql_query .= $post_type_query;
            $sql_query .= " order by post_year";
        }

        $years = $this->getResults($sql_query);

        if (is_int($limit) && count($years) > intval($limit, 10)) {
            $sql_query .= ' LIMIT ' . intval($limit, 10);
            $sql_query .= " UNION select 'Archive' AS post_year ";
        }

        return $sql_query;
    }

    /**
     * Get the query string for dates
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getDateQuery(Array $includes, $slug, $post_status, Array $queries)
    {

        $post_types = implode("','", $includes['types']);

        if (isset($includes[$slug . '_input_type']) && $includes[$slug . '_input_type'] === 'slider') {
            $sql_query = <<<SQL
                        (
                            SELECT DISTINCT
                                CONCAT('gt' ,LEFT(p2.meta_value, 4), RIGHT(p2.meta_value, 2)) AS slug,
                                CONCAT(LEFT(p2.meta_value, 4), '/', RIGHT(p2.meta_value, 2)) AS filter_name
                            FROM wp_postmeta AS p2, wp_posts
                            WHERE
                                meta_key = '{$includes[$slug . '_meta_key']}'
                                AND post_id = ID
                                AND post_type IN ('{$post_types}')
                                AND post_status IN ({$post_status})
                                {$queries['filter_query']}
                            ORDER BY meta_value ASC
                            LIMIT 1
                        )
                        UNION
                        (
                            SELECT DISTINCT
                                CONCAT('lt', LEFT(p2.meta_value, 4), RIGHT(p2.meta_value, 2)) AS slug,
                                CONCAT(LEFT(p2.meta_value, 4), '/', RIGHT(p2.meta_value, 2)) AS filter_name
                            FROM wp_postmeta AS p2, wp_posts
                            WHERE
                                meta_key = '{$includes[$slug . '_meta_key']}'
                                AND post_id = ID
                                AND post_type IN ('{$post_types}')
                                AND post_status IN ({$post_status})
                                {$queries['filter_query']}
                            ORDER BY meta_value DESC
                            LIMIT 1
                        )
SQL;
        } else {
            $sql_query = <<<SQL
                        SELECT DISTINCT
                            LEFT(p2.meta_value, 4) AS slug,
                            LEFT(p2.meta_value, 4) AS filter_name
                        FROM wp_postmeta AS p2
                        LEFT JOIN wp_posts AS p
                            ON p2.post_id = p.ID
                        WHERE
                            p2.meta_key {$queries['meta_key_query']}
                            {$queries['post_type_query']}
                        ORDER BY slug ASC
SQL;
        }

        return $sql_query;
    }

    /**
     * Get the query string for the date archive
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getDateArchiveQuery(Array $includes, $slug, $post_status, Array $queries)
    {

        if (isset($includes[$slug . '_archive_limit'])) {
            $limit = $includes[$slug . '_archive_limit'];
        } else {
            $limit = 5;
        }
        if (isset($includes[$slug . '_format'])) {
            $format = $includes[$slug . '_format'];
        } else {
            $format = 4;
        }

        if ($format === 'full') {
            $sql_query = <<<SQL
                        (
                            SELECT DISTINCT
                                p2.meta_value AS slug,
                                CONCAT(LEFT(p2.meta_value, 4), "/", RIGHT(p2.meta_value, 2))  AS filter_name
                            FROM wp_postmeta AS p2
                            LEFT JOIN wp_posts AS p
                                ON p2.post_id = p.ID
                            WHERE
                                p2.meta_key {$queries['meta_key_query']}
                                {$queries['post_type_query']}
                            ORDER BY slug DESC
                            LIMIT {$limit}
                        )
                        UNION
                        (
                            SELECT
                                'archive' AS slug,
                                'Archive' AS filter_name
                        )
SQL;
        } else {
            $sql_query = <<<SQL
                        (
                            SELECT DISTINCT
                                LEFT(p2.meta_value, 4) AS slug,
                                LEFT(p2.meta_value, 4) AS filter_name
                            FROM wp_postmeta AS p2
                            LEFT JOIN wp_posts AS p
                                ON p2.post_id = p.ID
                            WHERE
                                p2.meta_key {$queries['meta_key_query']}
                                {$queries['post_type_query']}
                            ORDER BY slug DESC
                            LIMIT {$limit}
                        )
                        UNION
                        (
                            SELECT
                                'archive' AS slug,
                                'Archive' AS filter_name
                        )
SQL;
        }

        return $sql_query;
    }

    /**
     * Get the query string for the amount ranges
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getAmountRangeQuery(Array $includes, $slug, $post_status, Array $queries)
    {

        $post_types = implode("','", $includes['types']);

        if (isset($includes[$slug . '_input_type']) && $includes[$slug . '_input_type'] === 'slider') {
            $sql_query = <<<SQL
                        (
                            SELECT
                                CONCAT('gt', MIN(CAST(meta_value AS signed))) AS slug,
                                MIN(CAST(meta_value AS signed)) AS filter_name
                            FROM wp_postmeta, wp_posts
                            WHERE
                                meta_key = '{$includes[$slug . '_meta_key']}'
                                AND post_id = ID
                                AND post_type IN ('{$post_types}')
                                AND post_status IN ({$post_status})
                                {$queries['filter_query']}
                        )
                        UNION
                        (
                            SELECT
                                CONCAT('lt', MAX(CAST(meta_value AS signed))) AS slug,
                                MAX(CAST(meta_value AS signed)) AS filter_name
                            FROM wp_postmeta, wp_posts
                            WHERE
                                meta_key = '{$includes[$slug . '_meta_key']}'
                                AND post_id = ID
                                AND post_type IN ('{$post_types}')
                                AND post_status IN ({$post_status})
                                {$queries['filter_query']}
                        )
SQL;
        } else {
            if (isset($includes[$slug . '_min'])) {
                $min = $includes[$slug . '_min'];
            } else {
                $min = '10000';
            }

            if (isset($includes[$slug . '_max'])) {
                $max = $includes[$slug . '_max'];
            } else {
                $max = '300000';
            }

            if (isset($includes[$slug . '_range_values'])) {
                $ranges = $includes[$slug . '_range_values'];
            } else {
                $ranges = [
                    '10000-50000',
                    '50000-100000',
                    '100000-200000',
                    '200000-300000'
                ];
            }

            $range_values[] = [
                'slug' => 'lt' . substr(str_replace(",", "", $min), 0, -3) . 'k',
                'name' => 'Less than ' . utf8_encode(money_format('%.0n', ceil(floatval(str_replace(",", "", $min)))))
            ];

            foreach ($ranges as $range) {
                $range  = explode('-', $range);
                $value1 = $range[0];
                $value2 = $range[1];

                $range_values[] = [
                    'slug' => 'gt' . substr($value1, 0, -3) . 'k' . 'lt' . substr($value2, 0, -3) . 'k',
                    'name' => utf8_encode(money_format('%.0n', ceil(floatval(str_replace(",", "", $value1))))) . '-' . utf8_encode(money_format('%.0n', ceil(floatval(str_replace(",", "", $value2)))))
                ];
            }

            $range_values[] = [
                'slug' => 'gt' . substr(str_replace(",", "", $max), 0, -3) . 'k',
                'name' => 'Above ' . utf8_encode(money_format('%.0n', ceil(floatval(str_replace(",", "", $max)))))
            ];

            $range_values = array_reverse($range_values);

            $sql_query_array = [];

            foreach ($range_values as $range_value) {
                $sql_query_array[] = <<<SQL
                            (
                                SELECT
                                    '{$range_value['slug']}' AS slug,
                                    '{$range_value['name']}' AS filter_name
                            )
SQL;
            }

            $sql_query = implode(" UNION ", $sql_query_array);
        }

        return $sql_query;
    }

    /**
     * Get the query string for countries
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getCountryQuery(Array $includes, $slug, $post_status, Array $queries)
    {

        $sort = 'asc';

        if (!empty($includes[$slug . '_sort']) && strtolower($includes[$slug . '_sort']) === 'desc') {
            $sort = 'desc';
        }

        $sql_query = <<<SQL
                    SELECT DISTINCT
                        meta_value AS filter_name,
                        meta_value as slug
                    FROM wp_postmeta
                    WHERE
                        meta_key = '{$includes[$slug . '_meta_key']}'
                    ORDER BY filter_name {$sort}
SQL;

        return $sql_query;
    }

    /**
     * Get the query string for year differences
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getYearDiffQuery(Array $includes, $slug, $post_status, Array $queries)
    {

        $post_types = implode("','", $includes['types']);

        $sql_query = <<<SQL
                         SELECT DISTINCT
                            (year(now()) - meta.meta_value) AS cf_id,
                            (year(now()) - meta.meta_value) AS filter_name,
                            (year(now()) - meta.meta_value) AS slug
                         FROM wp_postmeta AS meta
                         LEFT JOIN wp_posts AS p2
                            ON meta.post_id = p2.ID
                         WHERE
                            meta_value != ''
                            AND meta_key {$queries['meta_key_query']}
                            AND p2.post_type IN ('{$post_types}')
                            {$queries['filter_query']}
                            {$queries['category_query']}
                            {$queries['post_type_query']}
                         ORDER BY filter_name ASC
SQL;

        return $sql_query;
    }

    /**
     * Get the query string for an unspecified custom field
     *
     * @param array  $includes    Array of settings for the filters
     * @param string $slug        Used to retrieve specific items from the $includes array
     * @param string $post_status The current post status query
     * @param array  $queries     Array of query strings already compiled for use
     *
     * @return string Post type query string
     */
    public function getCustomFieldQuery(Array $includes, $slug, $post_status, Array $queries)
    {

        $post_types = implode("','", $includes['types']);

        // Find authors in these types only
        if (!empty($includes[$slug . '_post_types'])) {
            $sql_query = <<<SQL
                        SELECT
                            p.post_title AS filter_name,
                            p.post_name AS slug
                        FROM wp_posts AS p
                        WHERE
                            p.post_status IN ({$post_status})
                            AND p.ID IN
                                (
                                    SELECT DISTINCT
                                        meta.meta_value AS cf_id
                                    FROM wp_postmeta AS meta
                                    LEFT JOIN wp_posts AS p2
                                        ON meta.post_id = p2.ID
                                    WHERE
                                        meta_key {$queries['meta_key_query']}
                                        AND p2.post_type IN ('{$post_types}')
                                        {$queries['filter_query']}
                                )
                            {$queries['category_query']}
                            {$queries['post_type_query']}
                        ORDER BY filter_name ASC
SQL;
        } else {
            $sql_query = <<<SQL
                        SELECT DISTINCT
                            meta.meta_value AS cf_id,
                            meta.meta_value AS filter_name,
                            meta.meta_value AS slug
                        FROM wp_postmeta AS meta
                        LEFT JOIN wp_posts AS p2
                            ON meta.post_id = p2.ID
                        WHERE
                            meta_value != ''
                            AND meta_key {$queries['meta_key_query']}
                            AND p2.post_type IN ('{$post_types}')
                            {$queries['filter_query']}
                            {$queries['category_query']}
                            {$queries['post_type_query']}
                        ORDER BY filter_name ASC
SQL;
        }

        return $sql_query;
    }
}
